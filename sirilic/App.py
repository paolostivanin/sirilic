#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL script.
# It can also group scripts.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import wx
import os
import sys
import sirilic.lib.tools as tools
import sirilic.ui.gui as gui
import sirilic.lib.callbacks as callbacks
import sirilic.lib.database as database
import sirilic.lib.prefs as prefs

# ==============================================================================


class App(wx.App):
    def OnInit(self):
        # Configuration
        if os.name == 'nt':
            self.myhome = os.environ.get('USERPROFILE')
        else:
            self.myhome = os.environ.get('HOME')

        i_prefs = prefs.CPrefs(self.myhome , '.sirilic2_rc')
        i_db = database.CDatabase()
        i_gui = gui.CGui(None, -1, "")  # instance Sirilic GUI
        self.cb = callbacks.CCallbacks(i_gui, i_db, i_prefs)
        
        # add icon
        _icon = wx.NullIcon
        _icon.CopyFromBitmap(wx.Bitmap(tools.resource_path(os.path.join(".","icon","cp-nb.ico"), pathabs=os.path.dirname(os.path.abspath(__file__)) ), wx.BITMAP_TYPE_ICO))
        i_gui.SetIcon(_icon)

        i_gui.Show(True)
        self.SetTopWindow(i_gui)

        return True


# -----------------------------------------------------------------------------
def Run():
    if sys.version_info[0] != 3 :
        print("*")
        print("* Python version :", sys.version_info)
        print("* Error, python version should be >= 3 ")
        print("*")
        exit()
    app = App(0)
    app.MainLoop()


if __name__ == '__main__':
    Run()
