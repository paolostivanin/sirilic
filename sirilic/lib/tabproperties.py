# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL script.
# It can also group scripts.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
from sirilic.lib.constantes import *

# ==============================================================================
class TabProperties:
    def __init__(self, i_gui, i_prefs ):

        self.i_prop = i_gui.wproperties
        self.prefs  = i_prefs
        
        self.Set_Option_WFHM_filter(i_prefs.Get('siril_dev'))
        if sys.platform.startswith('win32') :
            # gestion des spinctrl avec une taille adaptee a Windows
            width = 65
            self.i_prop.e_RejH_Image.SetMinSize((width, -1))
            self.i_prop.e_RejL_Image.SetMinSize((width, -1))
            self.i_prop.e_RejH_Cosmetic.SetMinSize((width, -1))
            self.i_prop.e_RejL_Cosmetic.SetMinSize((width, -1))
            self.i_prop.e_sigma_findstar.SetMinSize((width, -1))
            self.i_prop.e_roundness_findstar.SetMinSize((width, -1))
            self.i_prop.e_RejH_Offset.SetMinSize((width, -1))
            self.i_prop.e_RejL_Offset.SetMinSize((width, -1))
            self.i_prop.e_RejH_Flat.SetMinSize((width, -1))
            self.i_prop.e_RejL_Flat.SetMinSize((width, -1))
            self.i_prop.e_RejH_Dark.SetMinSize((width, -1))
            self.i_prop.e_RejL_Dark.SetMinSize((width, -1))
            self.i_prop.e_RejH_DFlat.SetMinSize((width, -1))
            self.i_prop.e_RejL_DFlat.SetMinSize((width, -1))

    def  Set_Option_WFHM_filter(self,enable):
        if enable :
            self.i_prop.e_wfwhm_filter.Enable()
        else:
            self.i_prop.e_wfwhm_filter.Disable()
            
    # --------------------------------------------------------------------------
    # charge les proprietes dans l'onglet 
    def Set(self,iodf):
        # Images Properties
        img_prop = iodf['Images'][IMAGE].GetData()
        self.i_prop.cb_OffsetSub_Image.SetValue(img_prop["suboffset"])
        self.i_prop.cb_drizzle.SetValue(        img_prop["Drizzle"  ])
        self.i_prop.cb_flip.SetValue(           img_prop["Flip"     ])
        self.i_prop.cb_optimdark.SetValue(      img_prop["DarkOpt"  ])
        self.i_prop.cb_StackImage.SetValue(     img_prop["Stack"    ])
        self.i_prop.e_RejH_Image.SetValue(      img_prop["RejH"     ])
        self.i_prop.e_RejL_Image.SetValue(      img_prop["RejL"     ])
        self.i_prop.cb_NormImage.SetValue(      img_prop["Norm"     ])
        self.i_prop.e_fwhm_filter.SetValue(     img_prop["fwhm"     ])
        self.i_prop.e_wfwhm_filter.SetValue(    img_prop["wfwhm"    ])
        self.i_prop.e_round_filter.SetValue(    img_prop["round"    ])
        self.i_prop.e_quality.SetValue(         img_prop["quality"  ])
        self.i_prop.cb_Cosmetic_Image.SetValue( img_prop["Cosmetic" ])
        self.i_prop.e_RejH_Cosmetic.SetValue(   img_prop["Hot"      ])
        self.i_prop.e_RejL_Cosmetic.SetValue(   img_prop["Cold"     ])
        self.i_prop.cb_findstar.SetValue(       img_prop["Findstar" ])
        self.i_prop.e_sigma_findstar.SetValue(  img_prop["Ksigma"   ])
        self.i_prop.e_roundness_findstar.SetValue( img_prop["Roundness" ])
        # Offsets Properties
        offset_prop = iodf['Images'][OFFSET].GetData()
        self.i_prop.cb_cpylibOffset.SetValue(   offset_prop["copylib"] )
        self.i_prop.cb_StackOffset.SetValue(    offset_prop["Stack"  ] )
        self.i_prop.e_RejH_Offset.SetValue(     offset_prop["RejH"   ] )
        self.i_prop.e_RejL_Offset.SetValue(     offset_prop["RejL"   ] )
        self.i_prop.cb_NormOffset.SetValue(     offset_prop["Norm"   ] )
        # Darks Properties
        dark_prop = iodf['Images'][DARK].GetData()
        self.i_prop.cb_OffsetSub_Dark.SetValue(dark_prop['suboffset'])
        self.i_prop.cb_cpylib_Dark.SetValue(   dark_prop['copylib'  ])
        self.i_prop.cb_StackDark.SetValue(     dark_prop['Stack'    ])
        self.i_prop.e_RejH_Dark.SetValue(      dark_prop['RejH'     ])
        self.i_prop.e_RejL_Dark.SetValue(      dark_prop['RejL'     ])
        self.i_prop.cb_NormDark.SetValue(      dark_prop['Norm'     ])
        # Flats Properties
        flat_prop = iodf['Images'][FLAT].GetData()
        self.i_prop.cb_OffsetSub_Flat.SetValue(flat_prop['suboffset'])
        self.i_prop.cb_cpylib_Flat.SetValue(   flat_prop['copylib'  ])
        self.i_prop.cb_StackFlat.SetValue(     flat_prop['Stack'    ])
        self.i_prop.e_RejH_Flat.SetValue(      flat_prop['RejH'     ])
        self.i_prop.e_RejL_Flat.SetValue(      flat_prop['RejL'     ])
        self.i_prop.cb_NormFlat.SetValue(      flat_prop['Norm'     ])
        # Dark-Flats Properties
        dflat_prop = iodf['Images'][DFLAT].GetData()
        self.i_prop.cb_OffsetSub_DFlat.SetValue(dflat_prop['suboffset'])
        self.i_prop.cb_cpylib_DFlat.SetValue(   dflat_prop['copylib'  ])
        self.i_prop.cb_StackDFlat.SetValue(     dflat_prop['Stack'    ])
        self.i_prop.e_RejH_DFlat.SetValue(      dflat_prop['RejH'     ])
        self.i_prop.e_RejL_DFlat.SetValue(      dflat_prop['RejL'     ])
        self.i_prop.cb_NormDFlat.SetValue(      dflat_prop['Norm'     ])

    # --------------------------------------------------------------------------
    # recupere les proprietes de l'onglet 
    def GetImage(self,iodf):
        img_prop = iodf['Images'][IMAGE].Recopie_data()
        img_prop["suboffset"] = self.i_prop.cb_OffsetSub_Image.GetValue()
        img_prop["Drizzle"  ] = self.i_prop.cb_drizzle.GetValue()
        img_prop["Flip"     ] = self.i_prop.cb_flip.GetValue()
        img_prop["DarkOpt"  ] = self.i_prop.cb_optimdark.GetValue()
        img_prop["Stack"    ] = self.i_prop.cb_StackImage.GetValue()
        img_prop["RejH"     ] = self.i_prop.e_RejH_Image.GetValue()
        img_prop["RejL"     ] = self.i_prop.e_RejL_Image.GetValue()
        img_prop["Norm"     ] = self.i_prop.cb_NormImage.GetValue()
        img_prop["fwhm"     ] = self.i_prop.e_fwhm_filter.GetValue()
        img_prop["wfwhm"    ] = self.i_prop.e_wfwhm_filter.GetValue()
        img_prop["round"    ] = self.i_prop.e_round_filter.GetValue()
        img_prop["quality"  ] = self.i_prop.e_quality.GetValue()
        img_prop["Cosmetic" ] = self.i_prop.cb_Cosmetic_Image.GetValue()
        img_prop["Hot"      ] = self.i_prop.e_RejH_Cosmetic.GetValue()
        img_prop["Cold"     ] = self.i_prop.e_RejL_Cosmetic.GetValue()
        img_prop["Findstar" ] = self.i_prop.cb_findstar.GetValue()
        img_prop["Ksigma"   ] = self.i_prop.e_sigma_findstar.GetValue()
        img_prop["Roundness"] = self.i_prop.e_roundness_findstar.GetValue()
        return img_prop    
        
    # --------------------------------------------------------------------------
    # recupere les proprietes de l'onglet 
    def GetOffset(self,iodf):
        offset_prop = iodf['Images'][OFFSET].Recopie_data()
        offset_prop["copylib"] = self.i_prop.cb_cpylibOffset.GetValue()
        offset_prop["Stack"  ] = self.i_prop.cb_StackOffset.GetValue()
        offset_prop["RejH"   ] = self.i_prop.e_RejH_Offset.GetValue()
        offset_prop["RejL"   ] = self.i_prop.e_RejL_Offset.GetValue()
        offset_prop["Norm"   ] = self.i_prop.cb_NormOffset.GetValue()
        return offset_prop
        
    def GetDark(self,iodf):
        dark_prop = iodf['Images'][DARK].Recopie_data()
        dark_prop['suboffset'] = self.i_prop.cb_OffsetSub_Dark.GetValue()
        dark_prop['copylib'  ] = self.i_prop.cb_cpylib_Dark.GetValue()
        dark_prop['Stack'    ] = self.i_prop.cb_StackDark.GetValue()
        dark_prop['RejH'     ] = self.i_prop.e_RejH_Dark.GetValue()
        dark_prop['RejL'     ] = self.i_prop.e_RejL_Dark.GetValue()
        dark_prop['Norm'     ] = self.i_prop.cb_NormDark.GetValue()
        return dark_prop
        
    def GetFlat(self,iodf):
        flat_prop = iodf['Images'][FLAT].Recopie_data()
        flat_prop['suboffset'] = self.i_prop.cb_OffsetSub_Flat.GetValue()
        flat_prop['copylib'  ] = self.i_prop.cb_cpylib_Flat.GetValue()
        flat_prop['Stack'    ] = self.i_prop.cb_StackFlat.GetValue()
        flat_prop['RejH'     ] = self.i_prop.e_RejH_Flat.GetValue()
        flat_prop['RejL'     ] = self.i_prop.e_RejL_Flat.GetValue()
        flat_prop['Norm'     ] = self.i_prop.cb_NormFlat.GetValue()
        return flat_prop
        
    def GetDFlat(self,iodf):
        dflat_prop = iodf['Images'][DFLAT].Recopie_data()
        dflat_prop['suboffset'] = self.i_prop.cb_OffsetSub_DFlat.GetValue()
        dflat_prop['copylib'  ] = self.i_prop.cb_cpylib_DFlat.GetValue()
        dflat_prop['Stack'    ] = self.i_prop.cb_StackDFlat.GetValue()
        dflat_prop['RejH'     ] = self.i_prop.e_RejH_DFlat.GetValue()
        dflat_prop['RejL'     ] = self.i_prop.e_RejL_DFlat.GetValue()
        dflat_prop['Norm'     ] = self.i_prop.cb_NormDFlat.GetValue()
        return dflat_prop
        