# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL script.
# It can also group scripts.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import os
import sys
import re
import subprocess
import threading
import ctypes
import gettext
import sirilic.lib.tools as tools
import sirilic.lib.actioncopy as actioncopy
import sirilic.lib.constantes as constantes

_ = gettext.gettext

# ==============================================================================
class SirilScript(threading.Thread):
    def __init__(self, log, siril_exe, script1, script2,
                       arbre, last_processed_image,
                       workdir, bCopy, bClean, bRunSiril, bDev ):
        threading.Thread.__init__(self)
        self.param=(log, siril_exe, script1, script2,
                       arbre, last_processed_image,
                       workdir, bCopy, bClean, bRunSiril, bDev )       

    # --------------------------------------------------------------------------
    def run(self):
        (log, siril_exe, script1, script2,
                       arbre, last_processed_image,
                       workdir, bCopy, bClean, bRunSiril, bDev ) = self.param
        task = tools.GestionThread()
        task.SetRunning(self)
        
        # ---------------------------------------------------------------------
        # verification de la compatibilite de Siril avec Sirilic
        if not os.path.exists( siril_exe ) :
            log.insert("***" + _("Error: siril don't exist") + " => " + siril_exe + '?\n')
            return

        try:
            rVers  =  eval( "[" + constantes.COMPATIBILITY_SIRIL[bDev].replace('.',',') + "]" )
            command = [  siril_exe ,'--version' ]
            try:
                if sys.platform.startswith('win32'):
                    SW_HIDE = 0
                    info = subprocess.STARTUPINFO()
                    info.dwFlags |= subprocess.STARTF_USESHOWWINDOW
                    info.wShowWindow = SW_HIDE
                    pipe_subprocess = subprocess.Popen(command, startupinfo=info, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.DEVNULL, cwd=None )
                else:
                    pipe_subprocess = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=None,bufsize=-1,universal_newlines=False )
            except Exception as e:
                log.insert("*** pipe_subprocess.Popen() :" + str( e ) + '\n')
                return

            try:
                version, errs = pipe_subprocess.communicate(timeout=15)
            except subprocess.TimeoutExpired:
                log.insert("***" + _("Timout expired : Problem to run 'Siril --version' ") +'\n')                
                pipe_subprocess.kill()
                return  
            except Exception as e:
                log.insert("*** pipe_subprocess.communicate() :" + str( e ) + '\n')
                return
            
            #version=b'SiriL is started as MacOS application\nsiril 0.99.4-19fa980\n' # pour check
            version = (version.decode('utf8')).split('\n')
            found = False
            for vv in version :
                if re.search(r'siril [0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*',vv) :
                    found = True
                    version = vv.strip() 
                    break
            if found == False :
                log.insert("*** Uncompatible version format :'" + str( version ) + "'\n")
                return
              
            version_split = version.split(' ')
            sirilname = version_split[0]
            number    = version_split[1]
            log.insert("VERSION "+version+' : ')
            (nMajor,nMinor,nRelease) = number.split('.')
            nRelease = nRelease.split('-')
            if type(nRelease) == list :
                nRelease=nRelease[0]

            compatible = False
            if int(nMajor) > rVers[0] :
                compatible = True
            else:
                if int(nMajor) == rVers[0] :
                    if int(nMinor) > rVers[1]  :
                        compatible = True
                    else:
                        if int(nMinor) == rVers[1]  :
                            if int(nRelease) >= rVers[2]:
                                compatible = True

            if compatible :
                log.insert(_("Siril is compatible with Sirilic ") +'\n')
            else:
                log.insert(_("Siril is not compatible with Sirilic ") +'\n')
                log.insert(_("Sirilic requires : Siril ") + str(rVers[0]) +'.'+ str(rVers[1]) +'.'+ str(rVers[2]) +'-'+ str(rVers[3]) +'\n')
                return
        except Exception as e:
            log.insert("***" + _("Error: Siril don't work") + " => " + siril_exe + '?\n')
            log.insert("*** " + str( e ) + '\n')
            return

        # ---------------------------------------------------------------------
        script1_cmd=[ siril_exe , '-s' , os.path.basename(script1) ]
        script2_cmd=[ siril_exe , '-s' , os.path.basename(script2) ]

        os.chdir(os.path.dirname(script1))
        if self.exec_command( log, script1_cmd,cwd=None ) :
            log.AbortMsg()
            return

        log.insert('\n')
        log.print_ligne('.')
        log.print_titre('.',_("SCRIPT-PART1 : FINISHED"))
        log.print_ligne('.')
        log.insert('\n')

        if actioncopy.Copy_fusion_session(log, arbre, workdir, flagCopy = bCopy, flagClean=bClean  ) :
            log.AbortMsg()
            return

        if task.IsAborted() :
            log.AbortMsg()
            return

        os.chdir(os.path.dirname(script2))
        if self.exec_command(log, script2_cmd ,cwd=None ) :
            log.AbortMsg()
            return

        log.insert('\n')
        log.print_ligne('.')
        log.print_titre('.',_("SCRIPT-PART2 : FINISHED"))
        log.print_ligne('.')
        log.SetStatus()

        if bRunSiril is True :
            run_alone(log, siril_exe,last_processed_image)

    # --------------------------------------------------------------------------
    def get_id(self):
        # returns id of the respective thread
        if hasattr(self, '_thread_id'):
            return self._thread_id
        for id, thread in threading._active.items():
            if thread is self:
                return id

    # --------------------------------------------------------------------------
    def raise_exception(self):
        thread_id = self.get_id()
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id,
              ctypes.py_object(SystemExit))
        if res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            print('Exception raise failure')

    # --------------------------------------------------------------------------
    def exec_command(self, log,command, cwd) :
        task = tools.GestionThread()
        log.insert("\n")
        if sys.platform.startswith('win32'):
            SW_HIDE = 0
            info = subprocess.STARTUPINFO()
            info.dwFlags |= subprocess.STARTF_USESHOWWINDOW
            info.wShowWindow = SW_HIDE
            pipe_subprocess = subprocess.Popen( command , startupinfo=info, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.DEVNULL, cwd=cwd )
        else:
            pipe_subprocess = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=cwd,bufsize=-1,universal_newlines=False )
    
        task.SetPipe(pipe_subprocess)
        out_thread = StdoutThread(log,pipe_subprocess)
        err_thread = StderrThread(log,pipe_subprocess)
        err_thread.start()
        out_thread.start()
        out_thread.join()
        err_thread.join()
        pipe_subprocess.communicate() # pour mettre ajour Pipe_subprocess.returncode
        returncode = pipe_subprocess.returncode
        task.SetPipe(None)
        return task.IsAborted() or (returncode != 0)

# ==============================================================================
class StdoutThread(threading.Thread):
    def __init__( self,log, pipe):
        self.log  = log
        self.pipe = pipe
        threading.Thread.__init__(self)
    def run(self):
        pipe =  self.pipe
        byte_buffer=b''
        byte=b'.'
        code='utf8'
        while (pipe.poll() is None)  and (byte !=b'') :
            byte = pipe.stdout.read(1)
            byte_buffer= byte_buffer+byte
            if byte == b'\n':
                buffer = byte_buffer.decode(code,errors='ignore')
                buffer = buffer.rstrip() + '\n'
                if not buffer[0].isalpha() :
                    buffer =  buffer[9:-1]
                if buffer[:8] == "progress:"[0:8] :
                    self.log.addProgress(buffer[:-1])
                else:
                    self.log.insert( buffer )
                byte_buffer=b''

class StderrThread(threading.Thread):
    def __init__( self,log,pipe):
        self.log  = log
        self.pipe = pipe
        threading.Thread.__init__(self)
    def run(self):
        pipe =  self.pipe
        byte_buffer=b''
        byte=b'.'
        code='utf8'
        #if sys.platform.startswith('win32') :
        #    code=locale.getpreferredencoding(True)
        while (pipe.poll() is None) and (byte !=b'') :
            byte =  pipe.stderr.read(1)
            byte_buffer=byte_buffer+byte
            if byte == b'\n':
                buffer = byte_buffer.decode(code,errors='ignore')
                buffer = "***" + buffer.rstrip() + '\n'
                self.log.insert( buffer )
                byte_buffer=b''



# ==============================================================================
def run_alone(log, siril_exe, filename=None):
    if (filename != None) :
        log.insert("*** " + _("last processed image") + " => " + filename + ' ***\n')

    log.print_titre('.',_("Run: ") + siril_exe + " ... " + _("waiting a few seconds") )

    try:
        cmd=[siril_exe]
        if (filename != None) :
            if ( len(filename) > 0 ):
                cmd.append(filename)
        subprocess.Popen(cmd)
    except Exception as e :
        log.insert( "***" + _("Error: Siril don't work") + " => " + siril_exe + '?\n')
        log.insert( "***  in run_alone(), subprocess.Popen() " + str(e) + '\n')
        return

