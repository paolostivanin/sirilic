# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL script.
# It can also group scripts.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import glob
import os
import gettext
import locale
import re

# ==============================================================================
def resource_path(relative_path, pathabs=None):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        #base_path = os.path.abspath(".")
        if pathabs== None :
            base_path = os.path.dirname( sys.argv[0] )
        else:
            base_path = pathabs

    return os.path.join(base_path, relative_path)

def init_language():
    try:
        if sys.platform.startswith('win'):
            if os.getenv('LANG') is None:
                lang, enc = locale.getdefaultlocale()
                os.environ['LANG'] = lang
                if len(enc) >0:
                    os.environ['LANG'] += "." + enc
        langue= os.environ['LANG'].split('.' )[0]
        if not re.search( "_" ,langue ) :
            langue = langue.lower() + "_" + langue.upper()
                
        path_abs=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        i18n_dir = resource_path( os.path.join('i18n' ,langue),pathabs=path_abs )

        if not os.path.exists(i18n_dir )  :
            gettext.install('sirilic')
        else:
            lang = gettext.translation('sirilic', os.path.dirname(i18n_dir) , languages=[langue,os.environ['LANG']] )
            lang.install()
    except Exception as e :
        print("error init_language():" + str(e))
        gettext.install('sirilic')

init_language()

# ==============================================================================
Abort_thread   =False
Running_thread =None
Pipe_subprocess=None

class GestionThread:
    def __init__(self):
        pass
        
    def IsAborted(self):
        global Abort_thread
        return Abort_thread;
    
    def IsRunning(self):
        global Running_thread
        if Running_thread is not None :
            if Running_thread.is_alive() :
                return True ;
        return False
    def SetRunning(self, value):
        global Running_thread
        global Pipe_subprocess
        global Abort_thread
        Running_thread = value         
        Abort_thread   = False
        Pipe_subprocess= None
        
    def SetPipe(self,pipe_value):
        global Pipe_subprocess
        Pipe_subprocess=pipe_value
        
    def Abort(self) :
        global Abort_thread
        global Pipe_subprocess
        if self.IsRunning() :
                Abort_thread=True
        if Pipe_subprocess is not None :
            Pipe_subprocess.kill()    
# ==============================================================================
def Stringify(data,ident1=4,ident2=8):
    IDENT1 = " " * ident1
    IDENT2 = " " * ident2
    str_data=str(data).replace(',',',\n'+IDENT2)
    str_data=str_data.replace('{',IDENT1+'{\n '+IDENT2)
    str_data=str_data.replace('}','\n'+IDENT1+'}')
    return str_data

# ------------------------------------------------------------------------------
def String2data( lines ) :
    str_data=""
    for chaine in lines:
        chaine=chaine.lstrip()
        chaine=chaine.rstrip()
        if not chaine :
            continue
        if (chaine[0] == "#" ) :
            continue # skip comments
        str_data= str_data + chaine
    return eval(str_data)
# ==============================================================================
def path_conv( path ):
    return os.path.normpath( path )

# ------------------------------------------------------------------------------
def mkdirs(name):
    # pour compatibilite Python 2.7
    path= path_conv(name)
    if not os.path.exists(path):
        os.makedirs(path)

# ------------------------------------------------------------------------------
def isFitExt(extension ):
    if extension.lower() == ".fit" :
        return True
    if extension.lower() == ".fts" :
        return True
    if extension.lower() == ".fits" :
        return True
    return False

# ------------------------------------------------------------------------------
def isFitFile(filename ):
    xx,extension=os.path.splitext(filename)
    if extension.lower() == ".fit" :
        return True
    if extension.lower() == ".fts" :
        return True
    if extension.lower() == ".fits" :
        return True
    return False

# ------------------------------------------------------------------------------
def nettoyage_dossier(log, dossier, ext=None, prefix=None) :
    log.print_ligne("-")
    log.print_titre('-',_('Cleaning : ')  + dossier)

    if not os.path.exists( dossier ):
        log.print_titre('-',"directory doesn't exist ... skipping")
        return

    if prefix == None :
        prefix=[""]

    for pp in prefix:
        # detruit les fichiers dans la destination avant de faire la copie
        remove_file( log, dossier + os.sep + pp +  "*.fit"  )
        remove_file( log, dossier + os.sep + pp +  "*.fits" )
        remove_file( log, dossier + os.sep + pp +  "*.fts"  )
        remove_file( log, dossier + os.sep + pp +  "*.seq"  )
        if ext != None :
            remove_file( log, dossier + os.sep + pp +  "*" + ext )

# ------------------------------------------------------------------------------
def remove_file( log, pattern ):
    liste=glob.glob( pattern )
    liste.sort()
    for fic in liste:
        cmd= "Remove "+ fic
        log.insert(cmd + '\n')
        try:
            os.remove(fic)
        except Exception as e :
            log.insert("... aborted")
            log.insert("*** remove_file() :" + str( e ) + '\n')
            
