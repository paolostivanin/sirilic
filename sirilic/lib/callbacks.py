# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL script.
# It can also group scripts.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import wx
import wx.lib.newevent

import sys
import os
import glob
import time
import gettext
import sirilic.lib.changelog as changelog
import sirilic.ui.gui as gui
import sirilic.lib.tools as tools
import sirilic.lib.buildscript as buildscript
import sirilic.lib.tabprocessus as tabprocessus
import sirilic.lib.tabfiles as tabfiles
import sirilic.lib.tabproperties as tabproperties
import sirilic.lib.tablog as tablog
import sirilic.lib.actionsiril as actionsiril
import sirilic.lib.actioncopy as actioncopy
import sirilic.lib.actionclrtmp as actionclrtmp
from sirilic.lib.constantes import *

_ = gettext.gettext

# ==============================================================================

COL_OBJECT   = 0
COL_LAYER    = 1
COL_SESSION  = 2
COL_STATUS   = 3
NB_COL       = 4

# ==============================================================================
def darkRowFormatter(listctrl, dark=False, toggle=0 ):
    listItems = [listctrl.GetItem(i) for i
                 in range(listctrl.GetItemCount())]
    bgcolor = ( "Light Blue" , "white" )
    fgcolor = ( "black" , "black" )
    if dark:
        bgcolor = ( "Dark Grey",  "Light Grey" )
        fgcolor = ( "white",  "black" )
    if toggle != 0 :
        toggle = 1

    for index, item in enumerate(listItems):
        item.SetBackgroundColour(bgcolor[toggle])
        item.SetTextColour(fgcolor[toggle])
        listctrl.SetItem(item)
        toggle = 1 - toggle

# ==============================================================================
class CCallbacks():
    def __init__(self, i_gui, i_db , i_prefs ):
        self.th_run      = None
        self.i_gui       = i_gui
        self.i_proc      = i_gui.wprocess
        self.i_log       = i_gui.wlog
        self.i_db        = i_db
        self.prefs       = i_prefs
        self.listProject = i_gui.listProject
        
        self.prefs.Load()

        self.i_tabfiles   = tabfiles.TabFiles(i_gui,i_prefs, self.ChangeOnglet )
        self.i_properties = tabproperties.TabProperties(i_gui,i_prefs)        
        self.log          = tablog.LogConsole(self.i_log.tlog,self.i_log.lProgress,self.i_gui.lTraitement,self.i_gui.gProgress)

        self.log.LogTrace(True,self.prefs.Get('workdir'))

        self.BaseTitle  = i_gui.GetTitle()
        self.mem_stdout = sys.stdout
        self.mem_stderr = sys.stderr
        self.fd_stddbg  = None
        self.DebugTrace(self.prefs.Get('debug'))

        self.CurrentProject    = ""
        self.CurrentImage      = ""
        self.bExitWin          = False
        self.MemoTreeStr       = ""
        self.MemoListProjetStr = ""
        self.ModifiedProject   = False
        self.ModifiedDB        = False
        
        self.MemoFileBox       = []

        # Event pour reconstruire la liste des images et l'arbre
        self.RebuildList, EVT_REBUILD_LIST = wx.lib.newevent.NewEvent()
        i_gui.Bind( EVT_REBUILD_LIST, self.CB_RebuildList )

        i_gui.Bind(wx.EVT_CLOSE, self.CB_closed )
        i_gui.Bind(wx.EVT_MENU,  self.CB_closed , id=wx.ID_EXIT        )

        # Add Menu
        menu = i_gui.menubar.mRunScript.GetMenu()
        mAbortAction = wx.MenuItem(menu, gui.myID_KILL, _("Abort Action"), _("Abort the current action"))
        mAbortAction.SetBackgroundColour("#FF8080")
        menu.AppendSeparator()
        menu.Append(mAbortAction)
        menu.AppendSeparator()
        menu.Append(gui.myID_BUILDSCRIPT, _("Display scripts "),
                                          _("Display the scripts without launching it"))
        menu.Append(gui.myID_PARTIAL_RUN, _("Run / fine tuning") + " (" +_("Experimental")+ ")",
                                          _("Launch part of the processing script for fine tuning"))
        menu.Append(gui.mID_RUNSIRIL, _("Run Siril (for manual processing)"),
                                      _("launches Siril to do the last treatments (color calibration, gradient, ...)"))
        menu.AppendSeparator()
        menu.Append(gui.myID_DEL_TMPFILE, _("Delete intermediate files"), _("Delete intermediate files from subfolders"))

        # bind with callback
        i_gui.Bind(wx.EVT_MENU,  self.CB_MenuNew       , id=wx.ID_NEW           )
        i_gui.Bind(wx.EVT_MENU,  self.CB_MenuAdd       , id=gui.myID_ADD        )
        i_gui.Bind(wx.EVT_MENU,  self.CB_MenuModify    , id=gui.myID_MODIFY     )
        i_gui.Bind(wx.EVT_MENU,  self.CB_MenuDel       , id=gui.myID_DEL        )
        i_gui.Bind(wx.EVT_MENU,  self.CB_LoadProject   , id=wx.ID_OPEN          )
        i_gui.Bind(wx.EVT_MENU,  self.CB_SaveProject   , id=wx.ID_SAVE          )
        i_gui.Bind(wx.EVT_MENU,  self.CB_SaveAsProject , id=wx.ID_SAVEAS        )
        i_gui.Bind(wx.EVT_MENU,  self.CB_Prefs         , id=wx.ID_PREFERENCES   )
        i_gui.Bind(wx.EVT_MENU,  self.CB_GlobProp      , id=gui.myID_GLOBPROP   )
        i_gui.Bind(wx.EVT_MENU,  self.CB_filecopy      , id=gui.myID_CPY        )
        i_gui.Bind(wx.EVT_MENU,  self.CB_run           , id=gui.myID_RUN        )
        i_gui.Bind(wx.EVT_MENU,  self.CB_kill          , id=gui.myID_KILL       )
        i_gui.Bind(wx.EVT_MENU,  self.CB_BuildScript   , id=gui.myID_BUILDSCRIPT)
        i_gui.Bind(wx.EVT_MENU,  self.CB_run_siril     , id=gui.mID_RUNSIRIL    )
        i_gui.Bind(wx.EVT_MENU,  self.CB_dbg_run_siril , id=gui.myID_PARTIAL_RUN)
        i_gui.Bind(wx.EVT_MENU,  self.CB_About         , id=wx.ID_ABOUT         )
        i_gui.Bind(wx.EVT_MENU,  self.CB_Clr_tmpfile   , id=gui.myID_DEL_TMPFILE)
        i_gui.Bind(wx.EVT_MENU,  self.CB_ReverseProject, id=gui.myID_REVERSE_PROJECT)
        i_gui.Bind(wx.EVT_MENU,  self.CB_DefaultMaster , id=gui.myID_DEFAULTMASTERS)

        for ii in range(gui.NB_HISTO):
            i_gui.Bind(wx.EVT_MENU,  self.CB_LastProject  , id=gui.myID_LASTPRJ[ii])

        self.i_log.bclear.Bind(wx.EVT_BUTTON   , self.CB_clearlog )

        i_gui.listProject.Bind(wx.EVT_LIST_ITEM_SELECTED  , self.CB_LoadProperties )
        i_gui.listProject.Bind(wx.EVT_LIST_ITEM_DESELECTED, lambda evt, clr=True  : self.CB_SaveProperties(evt,clr) )
        #i_gui.treeProject.Bind(wx.EVT_SET_FOCUS           , lambda evt, clr=False : self.CB_SaveProperties(evt,clr) )

        self.i_gui.SetAcceleratorTable(wx.AcceleratorTable([
                        (wx.ACCEL_NORMAL, wx.WXK_F1, wx.ID_EXIT ),
                        ]))

        
        self.menu_labels_by_id = {}
        self.menu_labels=[ _("Copy all"), _("Replace all"), _("Append"),_("Clear all") ]
        for title in self.menu_labels :
            self.menu_labels_by_id[ wx.NewId() ] = title
            
        # gestion manuelle des onglets car bug sur MAC OS
        i_gui.b_nb_process.Bind(   wx.EVT_TOGGLEBUTTON, lambda evt, target_No=0 : self.CB_Onglet(evt, target_No))
        i_gui.b_nb_files.Bind(     wx.EVT_TOGGLEBUTTON, lambda evt, target_No=1 : self.CB_Onglet(evt, target_No))
        i_gui.b_nb_properties.Bind(wx.EVT_TOGGLEBUTTON, lambda evt, target_No=2 : self.CB_Onglet(evt, target_No))
        i_gui.b_nb_log.Bind(       wx.EVT_TOGGLEBUTTON, lambda evt, target_No=3 : self.CB_Onglet(evt, target_No))
        self.onglets=[ ( i_gui.b_nb_process   , self.i_proc      ) ,
                       ( i_gui.b_nb_files     , i_gui.wfiles     ) ,
                       ( i_gui.b_nb_properties, i_gui.wproperties) ,
                       ( i_gui.b_nb_log       , self.i_log       ) ]

        self.i_tabfiles.ClrAllListBoxFiles()

        for ii in range(gui.NB_HISTO):
            self.i_gui.mLastProject[ii].SetItemLabel( _("empty history") )


        if type(self.prefs.Get('last_project')) is str :
            self.prefs.Set('last_project', [ self.prefs.Get('last_project') ] ) # retro compatibility

        if len(self.prefs.Get('last_project') ) ==  0 :
            self.prefs.Set('last_project', [ _("empty history") ] * gui.NB_HISTO )

        self.processus = tabprocessus.CProcessus(self.i_proc.f_draw,i_db)
        self.load_project( self.prefs.Get('last_project')[-1],False)
        self.ChangeOnglet(TAB_PROCESSUS)
        

# ------------------------------------------------------------------------------
    def SetProjectTitle(self, projectfilename ):
        if len(projectfilename)==0 :
            self.i_gui.SetTitle(self.BaseTitle)
        else:
            prjname=os.path.basename(projectfilename)
            if self.ModifiedProject :
                prjname+= " <" + _("modified") + ">"
            self.i_gui.SetTitle(self.BaseTitle + "           *** " + _("Project") + " : " + prjname  + " ***")

# ------------------------------------------------------------------------------
    def SaveAuto(self):
        self.ChangeOnglet(TAB_PROCESSUS)
        if self.ModifiedProject == True :
            if self.CurrentProject != "" :
                dlg = wx.MessageDialog(None, _("Do you save the current project?"),
                                       self.CurrentProject, wx.YES_NO | wx.ICON_QUESTION )
                if dlg.ShowModal() == wx.ID_YES:
                    self.save_project()
            else:
                self.CB_SaveAsProject(None)
                self.RefreshHistoProject()
        self.ModifiedProject = False
        self.ModifiedDB      = False

# ------------------------------------------------------------------------------
    def CB_closed(self,event):
        #print("Close Win")
        self.SaveAuto()
        self.DebugTrace(False)
        self.bExitWin = True
        self.prefs.Save()
        self.log.LogTrace(False,self.prefs.Get('workdir'))
        self.i_gui.Destroy()

# ------------------------------------------------------------------------------
    def CB_Onglet(self,event,target_No):
        target_obj,panel_obj = self.onglets[target_No]
        value=target_obj.GetValue()
        if value == True :
            for ii in range(4) :
                target_obj,panel_obj = self.onglets[ii]
                target_obj.InvalidateBestSize()

                if ii == target_No :
                    panel_obj.Show(True)
                else:
                    panel_obj.Show(False)
                    target_obj.SetValue(False)
            self.i_gui.SendSizeEvent()
            self.CB_ChangeTab(event)

    def ChangeOnglet(self,target_No):
        for ii in range(4) :
            target_obj,panel_obj = self.onglets[ii]
            target_obj.InvalidateBestSize()

            if ii == target_No :
                target_obj.SetValue(True)
                panel_obj.Show(True)
            else:
                panel_obj.Show(False)
                target_obj.SetValue(False)
        self.i_gui.SendSizeEvent()
        self.CB_ChangeTab(None)
# ------------------------------------------------------------------------------
    def CB_popupmenu(self, event, target_obj):
        menu = wx.Menu()
        for (id,title) in self.menu_labels_by_id.items():
            menu.Append( id, title )
            menu.Bind(wx.EVT_MENU,  lambda evt, idmenu=id, target_obj=target_obj : self.MenuSelectionCb(evt,idmenu,target_obj)  , id=id        )
        pos = event.GetPosition()
        pos = self.i_gui.ScreenToClient(pos)
        self.i_gui.PopupMenu( menu, pos )
        menu.Destroy()
            
    def MenuSelectionCb(self, evt, id, target_obj ):       
            
        if self.menu_labels_by_id[id] == self.menu_labels[0] : # "Copy all" 
            self.MemoFileBox =self.GetListBoxFiles( target_obj )
            
        if self.menu_labels_by_id[id] == self.menu_labels[1] : # "Replace all" 
            self.SetListBoxFiles( target_obj, self.MemoFileBox)
            
        if self.menu_labels_by_id[id] == self.menu_labels[2] : # "Append" 
            FileBox = self.GetListBoxFiles( target_obj ) + self.MemoFileBox
            FileBox = set( FileBox )
            self.SetListBoxFiles( target_obj, list(FileBox) )
            
        if self.menu_labels_by_id[id] == self.menu_labels[3] : # "Clear all" 
            target_obj.Clear()
            
# ------------------------------------------------------------------------------
    def GetPrefs(self) :
        ll=len(self.prefs.Get('last_project'))
        if ll > 0 :
            if ll > 5 :
                ll = 5
            self.prefs.Set('last_project', self.prefs.Get('last_project')[-ll:] )
        else:
            self.prefs.Set('last_project', "")
        return self.prefs

# ------------------------------------------------------------------------------
    def CB_ChangeTab(self, event):
        if (self.CurrentImage == "") or self.bExitWin :
            return
        self.save_properties( self.CurrentImage, False )
        self.UpdateStatus()
        self.i_proc.f_draw.Refresh()
        self.RefreshListTree(False)

    def CB_SaveProperties(self, event=None,clr=True):
        if self.CurrentImage == "" :
            return
        self.save_properties( self.CurrentImage,clr )
        self.CurrentImage = ""
        self.processus.SetCurrentImage("")
        self.i_proc.f_draw.Refresh()
        self.UpdateStatus()
        if self.ModifiedDB :
            evt = self.RebuildList(attr1="CB_SaveProp")
            wx.PostEvent(self.i_gui,evt)

    def CB_LoadProperties(self, event):
        item = self.listProject.GetFirstSelected()
        if ( item == -1 ):
            return
        object = self.listProject.GetItemText(item,0)
        layer  = self.listProject.GetItemText(item,1)
        session= self.listProject.GetItemText(item,2)
        SelectedImage = self.i_db.KeyStr(object,layer,session)
        if self.CurrentImage == SelectedImage:
            return
        if self.CurrentImage != "":
            self.save_properties( self.CurrentImage )
        self.load_properties( SelectedImage)
        self.CurrentImage = SelectedImage
        self.processus.SetCurrentImage(SelectedImage)
        self.UpdateStatus()
        self.i_proc.f_draw.Refresh()
        if self.ModifiedDB :
            evt = self.RebuildList(attr1="CB_LoadProp")
            wx.PostEvent(self.i_gui,evt)

    def CB_RebuildList(self,event ):
        self.RefreshListTree(False)

    def load_properties(self, imagename ):
        files = self.i_db.GetFiles(imagename)
        iodf  = self.i_db.GetItemDb(imagename)
        
        self.i_tabfiles.Set(files)
        self.i_properties.Set(iodf)

    def save_properties(self, imagename, clear=True ):
        files    = self.i_tabfiles.Get()
        modified = self.i_db.SetFiles(imagename,files)
        if clear :
            self.i_tabfiles.ClrAllListBoxFiles()
            
        iodf  = self.i_db.GetItemDb(imagename)       
        # Images Properties
        img_prop=self.i_properties.GetImage(iodf)
        modified |= iodf['Images'][IMAGE].SetData(img_prop)
        # Offsets Properties
        offset_prop=self.i_properties.GetOffset(iodf)
        modified |= iodf['Images'][OFFSET].SetData(offset_prop)
        # Darks Properties
        dark_prop=self.i_properties.GetDark(iodf)
        modified |= iodf['Images'][DARK].SetData(dark_prop)
        # Flats Properties
        flat_prop=self.i_properties.GetFlat(iodf)
        modified |= iodf['Images'][FLAT].SetData(flat_prop)
        # Dark-Flats Properties
        dflat_prop=self.i_properties.GetDFlat(iodf)
        modified |= iodf['Images'][DFLAT].SetData(dflat_prop)        
       
        if (self.ModifiedProject==False) and ( modified == True) :
            self.ModifiedProject |= modified
            self.SetProjectTitle(self.CurrentProject)
        self.ModifiedProject |= modified
        self.ModifiedDB       = modified

    def UpdateStatus( self ) :
        item=-1
        first=True
        while 1 :
            item = self.listProject.GetNextItem(item, wx.LIST_NEXT_ALL)
            if item == -1 :
                return
            ObjectName  = self.listProject.GetItemText(item,0)
            LayerName   = self.listProject.GetItemText(item,1)
            SessionName = self.listProject.GetItemText(item,2)
            keystr      = self.i_db.KeyStr(ObjectName,LayerName,SessionName)
            etat        = self.i_db.Item_IsInitialised(keystr)
            litem = wx.ListItem()
            litem.SetId(item)
            litem.SetText( etat )
            litem.SetColumn(3)
            self.listProject.SetItem(litem)
            first=False
# ------------------------------------------------------------------------------
    def GetLine( self, SelectedOnly=False ) :
        item = self.listProject.GetFirstSelected()
        if ( item == -1 )  and  SelectedOnly == False :
            item = self.listProject.GetNextItem(-1)
        if item == -1 :
            return []
        line=[]
        for ii in range(0,NB_COL):
            line.append(self.listProject.GetItemText(item,ii))
        return line

    def GetDlgNewProject( self, dlg ):
        objname   = dlg.eObject.GetLineText(0)
        session   = dlg.eSession.GetLineText(0)
        nbsession = dlg.eNbSession.GetValue()
        layer_list= []
        for name,layer in dlg.cbLayer.items() :
            if layer.GetValue() == 1 :
                layer_list.append(name)

        for nb in range(1,nbsession+1):
            for layername in layer_list :
                if nbsession==1 :
                    sessionname=session
                else:
                    sessionname="%s%02d" % (session,nb)
                                    
                omaster=self.prefs.Get('offsetmaster')
                dmaster=self.prefs.Get('darkmaster')
                keystr = self.i_db.KeyStr(objname,layername,sessionname)
                print( "layername:",layername, " sessionname:",sessionname, " keystr:", keystr)
                
                self.i_db.Add(keystr)
                files = self.i_db.GetFiles(keystr)
                if len(omaster)!=0 : 
                    files[OFFSET] = [ omaster ]
                if len(dmaster)!=0 : 
                    files[DARK]   = [ dmaster ]
                if (len(omaster)!=0) or (len(dmaster)!=0) :
                    self.i_db.SetFiles(keystr,files)
        self.MemoListProjetStr="" # force le refresh 
        self.MemoTreeStr      =""
        self.RefreshListTree()

    def CB_MenuNew(self, event):
        self.SaveAuto()
        with gui.CNewProject(None, -1, "") as dlg :
            if self.prefs.Get('siril_dev') :
                dlg.cbCHa.Enable()
                dlg.cbCHaOiii.Enable()
            else:
                dlg.cbCHa.SetValue(False)
                dlg.cbCHaOiii.SetValue(False)
                dlg.cbCHa.Disable()
                dlg.cbCHaOiii.Disable()

            if dlg.ShowModal() == wx.ID_CANCEL:
                return
            self.Unselect()
            self.CurrentProject  = ""
            self.ModifiedProject = True
            self.ModifiedDB      = False
            self.SetProjectTitle(self.CurrentProject)
            self.listProject.DeleteAllItems()
            self.i_db.Clr()
            self.GetDlgNewProject( dlg )

    def CB_MenuAdd(self, event):
        Old = self.GetLine()
        with gui.CModifyProject(None, -1, "") as dlg :
            if Old  :
                dlg.eObject.SetValue(Old[0])
                dlg.eSession.SetValue(Old[2])
                for name,layer in dlg.rbLayer.items() :
                    layer.SetValue(name == Old[1])
            if dlg.ShowModal() == wx.ID_CANCEL:
                return
            New = [ None ] * 3
            New[0] = dlg.eObject.GetValue()
            New[2] = dlg.eSession.GetValue()
            for name,layer in dlg.rbLayer.items() :
                if layer.GetValue() :
                    New[1] = name

            self.Unselect()
            keystr_n = self.i_db.KeyStr(New[0],New[1],New[2])
            item_n  = self.i_db.GetItemDb(keystr_n)
            if item_n :
                # exist deja pas d'ajout
                return
            self.i_db.Add(keystr_n)
            item_n  = self.i_db.GetItemDb(keystr_n)
            if Old  :
                # copie  de l'item selectionne
                keystr_o = self.i_db.KeyStr(Old[0],Old[1],Old[2])
                self.i_db.Recopie( keystr_n, keystr_o)
            self.CurrentImage =  ""
            self.RefreshListTree()

    def CB_MenuModify(self, event):
        Old = self.GetLine()
        if not Old  :
            return
        with gui.CModifyProject(None, -1, "") as dlg :
            if self.prefs.Get('siril_dev') :
                dlg.rbCHa.Enable()
                dlg.rbCHaO3.Enable()
            else:
                dlg.rbCHa.SetValue(False)
                dlg.rbCHaO3.SetValue(False)
                dlg.rbCHa.Disable()
                dlg.rbCHaO3.Disable()

            dlg.eObject.SetValue(Old[0])
            dlg.eSession.SetValue(Old[2])
            for name,layer in dlg.rbLayer.items() :
                layer.SetValue(name == Old[1])
            if dlg.ShowModal() == wx.ID_CANCEL:
                return
            New = [ None ] * 3
            New[0] = dlg.eObject.GetValue()
            New[2] = dlg.eSession.GetValue()
            for name,layer in dlg.rbLayer.items() :
                if layer.GetValue() :
                    New[1] = name
            if New == Old :
                return
            self.Unselect()
            keystr_n = self.i_db.KeyStr(New[0],New[1],New[2])
            keystr_o = self.i_db.KeyStr(Old[0],Old[1],Old[2])
            item_o  = self.i_db.GetItemDb(keystr_o)
            item_n  = self.i_db.GetItemDb(keystr_n)
            if not item_n :
                self.i_db.Add(keystr_n)
                item_n  = self.i_db.GetItemDb(keystr_n)
            item_n['Images'] = item_o['Images']
            self.i_db.DelItem(keystr_o)
            self.CurrentImage =  ""
            self.RefreshListTree()

    def CB_MenuDel(self, event):
        item = self.listProject.GetFirstSelected()
        if item == -1 :
            return
        info = self.GetLine()
        keystr = self.i_db.KeyStr(info[0],info[1],info[2])
        self.i_db.DelItem(keystr)
        self.CurrentImage =  ""
        self.RefreshListTree()

    def RefreshListTree(self, select_first=True):
        idx_item = self.listProject.GetFirstSelected()
        linesuniq = sorted(self.i_db.GetItems(), key=lambda xx: xx[0]+"%d" % NoLayer[xx[1]]+xx[2], reverse=True)
        linesuniq_rev = sorted(self.i_db.GetItems(), key=lambda xx: xx[0]+"%d" % NoLayer[xx[1]]+xx[2], reverse=False)

        # Rafraichissement de liste des images si necessaire
        MemoListProjetStr = ""
        for line in linesuniq_rev :
            keystr  = self.i_db.KeyStr(line[0], line[1],line[2])
            MemoListProjetStr += line[0]+line[1]+line[2]+line[3]
        if MemoListProjetStr != self.MemoListProjetStr :
            self.MemoListProjetStr = MemoListProjetStr
            self.listProject.DeleteAllItems()
            if sys.platform.startswith('win32'):
                for line in linesuniq :
                    self.listProject.Append(line)
            else:
                for line in linesuniq_rev :
                    self.listProject.Append(line)
            darkRowFormatter(self.listProject)
            if select_first :
                idx_item=0
            if idx_item >= 0 :
                self.listProject.Select(idx_item)
                self.listProject.Focus(idx_item)

        # Rafraichissement de l'arbre des images si necessaire
        MemoTreeStr=""
        for line in linesuniq_rev :
            keystr  = line[0]+line[1]+line[2]
            nbFiles = self.GetNbImages(line[0], line[1],line[2])
            MemoTreeStr+= keystr + str(nbFiles)
        if MemoTreeStr != self.MemoTreeStr :
            self.MemoTreeStr = MemoTreeStr
            self.i_gui.treeProject.DeleteAllItems()
            root = self.i_gui.treeProject.AddRoot("Work directory: " + self.prefs.Get('workdir') )
            for line in linesuniq_rev :
                nbFiles = self.GetNbImages(line[0], line[1],line[2])
                self.CreateTree( root, line[0], line[1],line[2], nbFiles )

        self.ModifiedDB = False

    def GetNbImages(self, objet, layer, session ) :
        keystr  = self.i_db.KeyStr(objet, layer, session)
        item    = self.i_db.GetItem(keystr)
        NbFiles = []
        for typeimg in IODF_SORTED :
            dbimg   = item.GetIodf(typeimg)
            NbFiles.append( len(dbimg.GetFilesExpanded()) )
        return NbFiles

    def CreateTree(self, root, objet, layer, session, nbFiles ) :
        child1 = self.CreateNoeud( objet , root  )
        child2 = self.CreateNoeud( layer  , child1)
        child3 = self.CreateNoeud( session, child2)
        self.i_gui.treeProject.Expand(root)
        self.i_gui.treeProject.Expand(child1)
        self.i_gui.treeProject.Expand(child2)
        #self.i_gui.treeProject.expand(child3)

        for no in range(NB_IODF) :
            nb = nbFiles[no]
            if nb != 0 :
                typeimg=IODF_SORTED[no]
                StrImage=str(nb) +" " + TYPENAME[typeimg].lower()
                self.CreateNoeud(StrImage ,child3 )

    def CreateNoeud(self, name, root):
        item,cookie = self.i_gui.treeProject.GetFirstChild(root)
        while item.IsOk() :
            label = self.i_gui.treeProject.GetItemText(item)
            if label == name :
                return item
            item,cookie = self.i_gui.treeProject.GetNextChild(root,cookie)
        return self.i_gui.treeProject.AppendItem(root,name)
# ------------------------------------------------------------------------------
    def CB_GlobProp(self, event):
        with gui.CGlobProp(None, -1, "") as dlg :
            prop=self.i_db.GetProp()
            dlg.cb_multisession.SetValue(    prop['multisession']  )
            dlg.cb_stack_intermed.SetValue(  prop['stack_intermed'])
            dlg.cb_stretch.SetValue(         prop['stretch']       )
            dlg.cb_CFAequa.SetValue(         prop['CFAequa']       )
            #dlg.cb_DslrDebayerised.SetValue( prop['DebayerDslr']   )
            
            if self.prefs.Get('siril_dev') :
                dlg.cb_stretch.Hide()
            else:
                dlg.cb_stretch.Show()
                
            if dlg.ShowModal() == wx.ID_CANCEL:
                return
            changed = self.i_db.SetProp( dlg.cb_multisession.GetValue(),
                               dlg.cb_stack_intermed.GetValue(),
                               dlg.cb_CFAequa.GetValue(),
                               dlg.cb_stretch.GetValue()                               
                               )
            if changed == True :
                self.ModifiedProject = True
                self.ModifiedDB      = True
                self.SetProjectTitle(self.CurrentProject)

# ------------------------------------------------------------------------------
    def CB_ReverseProject(self, event) :
        self.SaveAuto()
        dir_object = self.prefs.Get('workdir')
        with wx.DirDialog(None, 'Choose work directory', dir_object, style=wx.DD_DEFAULT_STYLE | wx.FD_FILE_MUST_EXIST ) as openDirDialog :
            if openDirDialog.ShowModal() == wx.ID_CANCEL:
                return
            dir_object = openDirDialog.GetPath()

        objectname = os.path.basename(dir_object)
        len_dobj=len(dir_object)

        self.Unselect()
        self.CurrentProject  = ""
        self.ModifiedProject = True
        self.ModifiedDB      = False
        self.SetProjectTitle(self.CurrentProject)
        self.listProject.DeleteAllItems()
        self.i_db.Clr()

        # --
        DirLayerName  = NoLayer.keys()
        dirs_layer = []
        for dd in os.listdir(dir_object) :
            if os.path.isdir(dir_object + os.sep + dd) :
                if dd in DirLayerName :
                    dirs_layer.append(os.path.join(dir_object,dd))
        #print(dirs_layer)

        dirs_session=[]
        for dl in dirs_layer :
            for dd in os.listdir(dl) :
                if os.path.isdir(dl + os.sep + dd) :
                    if dd != "GROUP" :
                        ss=os.path.join(dl,dd)
                        dirs_session.append(ss)
                        ss=ss[len_dobj+1:].split(os.sep)
                        keystr = self.i_db.KeyStr(objectname,ss[0],ss[1])
                        self.i_db.Add(keystr)
        #print(dirs_session)

        DirTypeName = TYPENAME
        #dirs=[]
        for ds in dirs_session :
            lfiles = [ None ] * NB_IODF
            for dd in os.listdir(ds) :
                if os.path.isdir(ds + os.sep + dd) :
                    if dd in DirTypeName :
                        #dirs.append(os.path.join(ds,dd))
                        prefix = os.path.join(ds,dd, dd.lower() +  "00001")
                        ldir = glob.glob( prefix + ".*" )
                        extSrc=".fit"
                        if (len(ldir) >= 2 ) :
                            for d in ldir :
                                ext= os.path.splitext(d)[1]
                                if ext != extSrc :
                                    extSrc = ext
                                    break
                        for nb in range(NB_IODF) :
                            if TYPENAME[nb] == dd  :
                                lfiles[nb] = [ os.path.join(ds,dd, dd.lower() + "*" + extSrc ) ]
                                break
            dm="MASTER"
            if os.path.isdir(ds + os.sep + dm) :
                for nb in range(NB_IODF) :
                    if lfiles[nb] == None :
                        dd=TYPENAME[nb]
                        fichier = os.path.join(ds,dm, "master-" + dd.lower() +  ".fit")
                        if os.path.exists( fichier ) :
                            lfiles[nb] = [ fichier ]

            ss=ds[len_dobj+1:].split(os.sep)
            keystr = self.i_db.KeyStr(objectname,ss[0],ss[1])
            self.i_db.SetFiles(keystr,lfiles)

        self.RefreshListTree()

    def CB_LastProject(self, event):
        self.ChangeOnglet(TAB_PROCESSUS)
        for ii in range(gui.NB_HISTO) :
            if ( event.GetId() == gui.myID_LASTPRJ[ii] ):
                self.load_project(self.i_gui.mLastProject[ii].GetItemLabel())
                return


    def CB_LoadProject(self, event):
        typefile="Project files |*.prj|All file|*.*"
        flag    = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
        defaultdir=os.path.join(self.prefs.Get('workdir'),"Config")
        with wx.FileDialog(None, "Open", defaultdir, "",typefile , flag) as openFileDialog :
            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                return
            self.ChangeOnglet(TAB_PROCESSUS)
            self.load_project(openFileDialog.GetPath())

    def load_project(self, filename, select_first=True):
        self.SaveAuto()
        if not os.path.exists(filename) :
            return
        with open(filename,"r") as fd :
            lines= fd.readlines()
            if self.i_db.String2data(lines) :
                return
            self.CurrentImage    = ""
            self.ModifiedProject = False
            self.ModifiedDB      = False
            self.CurrentProject  = filename
            self.SetProjectTitle(self.CurrentProject)
            self.RefreshListTree(select_first)
            self.RefreshHistoProject( )

    def RefreshHistoProject(self ):
        if self.CurrentProject != self.prefs.Get('last_project')[-1] :
            self.prefs.Get('last_project').append(self.CurrentProject)

        nb=min(gui.NB_HISTO,len(self.prefs.Get('last_project')))
        for ii in range(nb):
            jj=ii+1
            if len(self.prefs.Get('last_project')[-jj]) != 0 :
                self.i_gui.mLastProject[ii].SetItemLabel(self.prefs.Get('last_project')[-jj])

    def Unselect(self) :
        for xx in range(self.listProject.GetItemCount()):
            self.listProject.Select(xx,0)

    def CB_SaveProject(self, event):
        if self.CurrentProject == "" :
            self.CB_SaveAsProject(event)
            return
        self.save_project()
        self.Unselect()

    def CB_SaveAsProject(self, event):
        extprj=".prj"
        typefile="Project files |*"+ extprj + "|All file|*.*"
        flag    = wx.FD_SAVE
        defaultdir=os.path.join(self.prefs.Get('workdir'),"Config")
        tools.mkdirs(defaultdir)
        # Nomme le fichier par defaut avec l'objet de la première ligne
        item = self.listProject.GetFirstSelected()
        if item == -1 :
            info = ["noname"]
        else:
            info = self.GetLine()

        defaultcfg= info[0] + "_" + _("object") + extprj
        with wx.FileDialog(None, "Save As", defaultdir, defaultcfg,typefile , flag) as openFileDialog :
            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                return
            self.CurrentProject = openFileDialog.GetPath()
            if self.CurrentProject[-4:] != extprj :
                self.CurrentProject = self.CurrentProject + extprj
            self.save_project()
            self.Unselect()

    def save_project(self):
        self.CB_SaveProperties()
        with open(self.CurrentProject,"w") as fd :
            fd.write("# " + _("Don't edit the file") +"\n" )
            fd.write(self.i_db.Stringify())
            self.ModifiedProject = False
            self.ModifiedDB      = False
        self.SetProjectTitle(self.CurrentProject)

    def CB_Prefs(self, event):
        with gui.CPrefs(None, -1, "") as dlg :
            if sys.platform.startswith('win32') :                
                width = 65 # gestion des spinctrl avec une taille adaptee a Windows
                dlg.spCPU.SetMinSize((width, -1))
                dlg.spMem.SetMinSize((width, -1))
                dlg.sp_quantif.SetMinSize((width, -1))
                dlg.sp_hscale.SetMinSize((width, -1))
                dlg.spCPU.SetInitialSize( dlg.spCPU.GetSizeFromTextSize( dlg.spCPU.GetTextExtent("99")))
                dlg.spMem.SetInitialSize( dlg.spMem.GetSizeFromTextSize( dlg.spMem.GetTextExtent("9.99")))

            dlg.b_workdir.Bind( wx.EVT_BUTTON, lambda evt, target_obj=dlg.textWorkDir   : self.CB_SelectDir(evt, target_obj))
            dlg.b_sirilexe.Bind(wx.EVT_BUTTON, lambda evt, target_obj=dlg.textSirilExe  : self.CB_SelectExe(evt, target_obj))
            dlg.bDetect.Bind(wx.EVT_BUTTON, lambda evt, target_obj=dlg.textSirilExe  : self.CB_DetectExe(evt, target_obj))
            dlg.textWorkDir.SetValue(      self.prefs.Get('workdir')       )
            dlg.cb_WorkDirFinal.SetValue(  self.prefs.Get('workdirreturn') )
            dlg.cb_RunSirilAfter.SetValue( self.prefs.Get('siril_autorun') )
            dlg.textSirilExe.SetValue(     self.prefs.Get('sirilexe')      )
            dlg.cb_cleandir.SetValue(      self.prefs.Get('cleandir')      )
            dlg.cb_links.SetValue(         self.prefs.Get('links')         )
            dlg.spCPU.SetValue(            self.prefs.Get('nbcpu')         )
            dlg.spMem.SetValue(            self.prefs.Get('nbmem')         )
            dlg.cbDebug.SetValue(          self.prefs.Get('debug')         )
            
            dlg.cb_siril_dev.SetValue(     self.prefs.Get('siril_dev')     )
            dlg.cb_fitseq.SetValue(        self.prefs.Get('fitseq')        )
            dlg.cb_savetif.SetValue(       self.prefs.Get('savetif')       )
            dlg.cb_set32b.SetValue(        self.prefs.Get('float32b')      )
            dlg.cb_compress.SetValue(      self.prefs.Get('compress')      )
            dlg.cb_typecomp.SetValue(      self.prefs.Get('compress_type') )
            dlg.sp_quantif.SetValue(       self.prefs.Get('compress_quantif'))
            dlg.sp_hscale.SetValue(        self.prefs.Get('compress_hscale'))
            dlg.cb_siril_dev.Bind(wx.EVT_CHECKBOX, lambda evt, dlgbox=dlg : self.OnToggleEnable(evt, dlgbox) )
            self.OnToggleEnable(None,dlg)

            if dlg.ShowModal() == wx.ID_CANCEL:
                self.i_properties.Set_Option_WFHM_filter(self.prefs.Get('siril_dev'))
                return
            
            self.prefs.Set('workdir'         , dlg.textWorkDir.GetValue()      )
            self.prefs.Set('workdirreturn'   , dlg.cb_WorkDirFinal.GetValue()  )
            self.prefs.Set('siril_autorun'   , dlg.cb_RunSirilAfter.GetValue() )
            self.prefs.Set('sirilexe'        , dlg.textSirilExe.GetValue()     )
            self.prefs.Set('cleandir'        , dlg.cb_cleandir.GetValue()      )
            self.prefs.Set('links'           , dlg.cb_links.GetValue()         )
            self.prefs.Set('nbcpu'           , dlg.spCPU.GetValue()            )
            self.prefs.Set('nbmem'           , dlg.spMem.GetValue()            )
            
            siril_dev = dlg.cb_siril_dev.GetValue()                                
            self.prefs.Set('siril_dev'       , siril_dev  )
            self.prefs.Set('fitseq'          , dlg.cb_fitseq.GetValue()        )
            self.prefs.Set('savetif'         , dlg.cb_savetif.GetValue()       )
            self.prefs.Set('float32b'        , dlg.cb_set32b.GetValue()        )
            self.prefs.Set('compress'        , dlg.cb_compress.GetValue()      )
            self.prefs.Set('compress_type'   , dlg.cb_typecomp.GetValue()      )
            self.prefs.Set('compress_quantif', dlg.sp_quantif.GetValue()       )
            self.prefs.Set('compress_hscale' , dlg.sp_hscale.GetValue()       )
            
            if not siril_dev :
                self.prefs.Set('fitseq' ,False )
            
            self.prefs.Set('debug'         , dlg.cbDebug.GetValue()          )
            self.DebugTrace(self.prefs.Get('debug'))
            self.log.LogTrace(True,self.prefs.Get('workdir'))

    def OnToggleEnable(self, evt, dlgbox ) :
        if dlgbox.cb_siril_dev.GetValue() :
            dlgbox.cb_fitseq.Enable()
            dlgbox.cb_savetif.Enable()
            dlgbox.cb_set32b.Enable()
            dlgbox.cb_compress.Enable()
            dlgbox.cb_typecomp.Enable()
            dlgbox.sp_quantif.Enable()
            dlgbox.sp_hscale.Enable()
        else:
            dlgbox.cb_fitseq.Disable()
            dlgbox.cb_savetif.Disable()
            dlgbox.cb_set32b.Disable()
            dlgbox.cb_compress.Disable()
            dlgbox.cb_typecomp.Disable()
            dlgbox.sp_quantif.Disable()
            dlgbox.sp_hscale.Disable()
        self.i_properties.Set_Option_WFHM_filter(dlgbox.cb_siril_dev.GetValue())

    def DebugTrace(self, enable ) :
        if enable :
            try:
                self.fd_stddbg=open(os.path.join( self.prefs.Get('workdir'), "sirilic-trace.log" ),"w")
                sys.stdout = self.fd_stddbg
                sys.stderr = self.fd_stddbg
            except:
                self.fd_stddbg = None
        else:
            if self.fd_stddbg is not None :
                sys.stdout = self.mem_stdout
                sys.stderr = self.mem_stderr
                self.fd_stddbg.close()
                self.fd_stddbg = None

    def CB_SelectDir(self,event,target_obj):
        dir=target_obj.GetValue()
        if not os.path.exists(dir) :
            dir = self.prefs.Get('workdir')
        with wx.DirDialog(None, 'Choose work directory', dir, style=wx.DD_DEFAULT_STYLE | wx.FD_FILE_MUST_EXIST ) as openDirDialog :
            if openDirDialog.ShowModal() == wx.ID_CANCEL:
                return
            target_obj.SetValue( openDirDialog.GetPath())

    def CB_DetectExe(self,event,target_obj):
        siril_exe = None
        if sys.platform.startswith('win32'):
            siril_exe="C:\\Program Files\\SiriL\\bin\\siril.exe"
            if not os.path.exists( siril_exe ) :
                siril_exe="C:\\Program Files (x86)\\SiriL\\bin\\siril.exe"
        if sys.platform.startswith('darwin') :
            siril_exe="/Applications/Siril.app/Contents/Resources/bin/Siril"
            if not os.path.exists( siril_exe ) :
                siril_exe="/Applications/SiriL.app/Contents/MacOS/siril"    # SiriL Dev             
        if sys.platform.startswith('linux') :
           siril_exe="/usr/bin/siril"
           if not os.path.exists( siril_exe ) :
                siril_exe="/usr/local/bin/siril"
        if siril_exe != None :
            target_obj.SetValue( siril_exe )

    def CB_SelectExe(self,event,target_obj):
        dir=os.path.dirname(target_obj.GetValue())
        if not os.path.exists(dir) :
            dir = self.prefs.Get('workdir')
        flag     = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
        typefile = "All files|*.*;*"
        with wx.FileDialog(None, _('Choose Siril executable (full path)'), dir,"", typefile, flag ) as openFileDialog :
            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                return
            target_obj.SetValue( openFileDialog.GetPath())

    def CB_SelectMaster(self,event,target_obj):
        dir=os.path.dirname(target_obj.GetValue())
        if not os.path.exists(dir) :
            dir = self.prefs.Get('workdir')
        flag    = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
        typefile="Fit files |*.fit;*.fts;*.fits|All file|*.*"
        with wx.FileDialog(None, _('Choose Master File'), dir,"", typefile, flag ) as openFileDialog :
            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                return
            target_obj.SetValue( openFileDialog.GetPath())


# ------------------------------------------------------------------------------
    def CB_kill(self, event) :
        task = tools.GestionThread()
        task.Abort( )
        time.sleep(1)
        global thread_script
        if self.IsRunningThread() :
            self.th_run.raise_exception()
        
    def IsRunningThread(self):
        if self.th_run is not None :
            if self.th_run.is_alive() :
                return True ;
        return False
            
# ------------------------------------------------------------------------------

    def CB_Clr_tmpfile(self,event):
        self.ChangeOnglet(TAB_LOG)
        self.log.clear()
        self.i_gui.Update()
        if self.IsRunningThread() :
            self.log.insert("\n*** " +_("task already in progress") + "\n\n")
            return ;     
        self.th_run=actionclrtmp.ClrtmpThread(self.log, self.i_db , workdir=self.prefs.Get('workdir'))
        self.th_run.start()
        
    def CB_filecopy(self, event) :
        self.ChangeOnglet(TAB_LOG)
        self.log.clear()
        self.i_gui.Update()
        if self.IsRunningThread() :
            self.log.insert("\n*** " +_("task already in progress") + "\n\n")
            return ;     
            
        self.th_run=actioncopy.CopyThread(self.log, self.i_db , workdir=self.prefs.Get('workdir'),
                              flagCopy = not self.prefs.Get('links') ,
                              flagClean=self.prefs.Get('cleandir') ,
                              fitseq=self.prefs.Get('fitseq'))
        self.th_run.start()

    def CB_clearlog(self, event=None):
        self.log.clear()

    def CB_run(self, event) :
        self.ChangeOnglet(TAB_LOG)
        self.i_gui.Update()
        if self.IsRunningThread() :
            self.log.insert("\n*** " +_("task already in progress") + "\n\n")
            return ;
          
        arbre,script1,script2,last_processed_image=self.CB_BuildScript(None)
        siril_exe=self.prefs.Get('sirilexe')
        self.th_run = actionsiril.SirilScript(self.log,siril_exe,script1,script2,arbre,last_processed_image,
                                    self.prefs.Get('workdir'), not self.prefs.Get('links'),
                                    self.prefs.Get('cleandir'), self.prefs.Get('siril_autorun'),
                                    self.prefs.Get('siril_dev')
                                    )
        self.th_run.start()

    def CB_BuildScript(self, event=None):
        self.ChangeOnglet(TAB_LOG)
        self.log.clear()
        scriptdir=os.path.join(self.prefs.Get('workdir'),'script')
        tools.mkdirs( scriptdir )
        GProp=self.i_db.GetProp()
        builder=buildscript.CScriptBuilder(self.log, os.path.join(scriptdir,"sirilic.ssf"),
                                           self.prefs , self.i_db, GProp)
        arbre,script1,script2,last_processed_image = builder.Build()
        self.log.SetStepNumber( builder.getStepNumber( ) )

        if event is not None :
            self.log.insert('\n')
            self.log.print_ligne('.')
            self.log.print_titre('.',"Script: " + script1 )
            self.log.print_ligne('.')
            self.log.insert('\n')
            try:
                with open(script1, encoding='utf-8', mode='r', errors='ignore' ) as fd :
                    lines= fd.readlines()
                    for line in lines:
                        self.log.insert(line.rstrip('\r'))
            except Exception as e :
                self.log.insert("***" + _("Error: can't open") + " => " + script1 + '?\n')
                self.log.insert("*** CB_BuildScript() " + str(e) + '\n')

            self.log.insert('\n')
            self.log.print_ligne('.')
            self.log.print_titre('.',"Script: " + script2 )
            self.log.print_ligne('.')
            self.log.insert('\n')
            try:
                with open(script2, encoding='utf-8', mode='r', errors='ignore') as fd :
                    lines= fd.readlines()
                    for line in lines:
                        self.log.insert(line.rstrip('\r'))
            except Exception as e :
                self.log.insert("***" + _("Error: can't open") + " => " + script2 + '?\n')
                self.log.insert("*** CB_BuildScript() " + str(e) + '\n')

        self.log.insert('\n')
        self.log.print_ligne('.')
        self.log.print_titre('.',_("Script building: FINISHED"))
        self.log.print_ligne('.')
        self.log.insert('\n')

        return (arbre,script1,script2,last_processed_image)

    def CB_run_siril(self, event):
        self.ChangeOnglet(TAB_LOG)
        actionsiril.run_alone( self.log, self.prefs.Get('sirilexe'))

# ------------------------------------------------------------------------------
    def CB_dbg_run_siril(self, event):
        self.ChangeOnglet(TAB_LOG)
        arbre,script1,script2,last_processed_image=self.CB_BuildScript(None)
        tag_list=[]
        for script in ( script1, script2 ) :
            with open(script,"r") as fd :
                lines= fd.readlines()
                for line in lines :
                    if line[0:7] == "#TAG#{ "[0:7] :
                        tag_list.append(line[7:-5] )


        with gui.CTagProcess(None, -1, "") as dlg :
            dlg.cklb_tag.Clear()
            dlg.cklb_tag.Append(tag_list[:-1])
            #dlg.cklb_tag.SetCheckedItems(range(dlg.cklb_tag.GetCount()))
            dlg.bSelectAll.Bind(wx.EVT_BUTTON, lambda evt, cklb=dlg.cklb_tag : self.OnSelectAll(evt, cklb) )
            dlg.bUnSelectAll.Bind(wx.EVT_BUTTON, lambda evt, cklb=dlg.cklb_tag : self.OnDeselectAll(evt, cklb) )
            if dlg.ShowModal() == wx.ID_CANCEL:
                return
            uncheck_tag=[]
            #print( dlg.cklb_tag.GetCheckedStrings() )
            for ii in range(dlg.cklb_tag.GetCount()):
                if not dlg.cklb_tag.IsChecked(ii) :
                    uncheck_tag.append(dlg.cklb_tag.GetString(ii))

        comment=""
        script1_w  = script1[:-4] + "-partial.ssf"
        with open(script1_w,"w") as fdw :
            with open(script1,"r") as fdr :
                lines= fdr.readlines()
                for line in lines :
                    if line[0:7] == "#TAG#{ "[0:7] :
                        tag = line[7:-5]
                        if tag in uncheck_tag :
                            comment = "#"
                        else:
                            comment=""

                    if line[0:7] == "#TAG#} "[0:7]:
                        comment=""
                    fdw.write( comment + line )
        comment=""
        script2_w  = script2[:-4] + "-partial.ssf"
        with open(script2_w,"w") as fdw :
            with open(script2,"r") as fdr :
                lines= fdr.readlines()
                for line in lines :
                    if line[0:7] == "#TAG#{ "[0:7] :
                        tag = line[7:-5]
                        if tag in uncheck_tag :
                            comment = "#"
                        else:
                            comment=""

                    if line[0:7] == "#TAG#} "[0:7]:
                        comment=""
                    fdw.write( comment + line )
        siril_exe=self.prefs.Get('sirilexe')
        self.th_run = actionsiril.CThread(self.log,siril_exe,script1_w,script2_w,arbre,last_processed_image,
                                    self.prefs.Get('workdir'), not self.prefs.Get('links'),
                                    self.prefs.Get('cleandir'), self.prefs.Get('siril_autorun'),
                                    self.prefs.Get('siril_dev')
                                    )
        self.th_run.start()


    def OnSelectAll(self, event,cklb):
        cklb.SetCheckedItems(range(cklb.GetCount()))

    def OnDeselectAll(self, event,cklb):
        for ii in range(cklb.GetCount()) :
            cklb.Check(ii, check=False)

# ------------------------------------------------------------------------------
    def CB_DefaultMaster(self, event) :
        self.ChangeOnglet(TAB_PROCESSUS)
        self.i_gui.Update()
        self.Unselect()
        with gui.CDlgDefaultMaster(None, -1, "") as dlg :

            dlg.bOffsetMaster.Bind( wx.EVT_BUTTON, lambda evt, target_obj=dlg.tOffsetMaster : self.CB_SelectMaster(evt, target_obj))
            dlg.bDarkMaster.Bind(wx.EVT_BUTTON, lambda evt, target_obj=dlg.tDarkMaster   : self.CB_SelectMaster(evt, target_obj))
            dlg.tOffsetMaster.SetValue( self.prefs.Get('offsetmaster') )
            dlg.tDarkMaster.SetValue(   self.prefs.Get('darkmaster')   )

            if dlg.ShowModal() == wx.ID_CANCEL:
                return
            omaster=dlg.tOffsetMaster.GetValue()
            dmaster=dlg.tDarkMaster.GetValue()

            self.prefs.Set('offsetmaster', omaster )
            self.prefs.Set('darkmaster'  , dmaster )

            modified = False
            liste_keystr = self.i_db.GetKeyStrSorted()
            for imagename in  liste_keystr :
                files = self.i_db.GetFiles(imagename)
                files[OFFSET] = None
                files[DARK]   = None             
                if len(omaster)!=0 :
                    files[OFFSET] = [ omaster ]
                if len(dmaster)!=0 : 
                    files[DARK]   = [ dmaster ]
                self.i_db.SetFiles(imagename,files)
                modified = True
            if modified :
                self.ModifiedDB      = True
                self.ModifiedProject = True
                self.SetProjectTitle(self.CurrentProject)
                self.UpdateStatus()
# ------------------------------------------------------------------------------
    def CB_About(self, event) :
        msg=( _("SiriLic ( SiriL Image Converter) is a software for preparing\n" +
              "acquisition files (raw, Offset, Flat and Dark) for processing with SiriL software.\n\n" +
              "It does 4 things:\n\n" +
              " 1. Structuring the SiriL working directory into sub-folders\n" +
              " 2. Convert Raw, Offset, Dark or Flat files into SiriL sequence\n" +
              " 3. Automatically generate the SiriL script according to the files present and the options\n" +
              " 4. Preprocess directly the images\n\n" +
              "It can also process the multi-session\n\n"
              ) +
              "This program is provided without any guarantee.\n" +
              "The license is  LGPL-v3\n" +
              "For details, see GNU General Public License, version 3 or later.\n" +
              '"https://www.gnu.org/licenses/gpl.html"\n\n'+
              "by  M27trognondepomme -  May 2020 \n\n\n" + changelog.CHANGELOG )


        dlg=wx.MessageBox(msg , _('About') + ": SiriLic Version " + changelog.NO_VERSION ,  wx.OK  | wx.ICON_INFORMATION )

