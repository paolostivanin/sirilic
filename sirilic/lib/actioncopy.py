# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL script.
# It can also group scripts.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import wx
import os
import io
import sys
import glob
import shutil
import subprocess
import threading
import gettext

from sirilic.lib.constantes import *
from sirilic.lib.tools import mkdirs
from sirilic.lib.tools import isFitExt
from sirilic.lib.tools import nettoyage_dossier
from sirilic.lib.tools import GestionThread

_ = gettext.gettext

# ==============================================================================
class CopyThread(threading.Thread):
    def __init__( self, log, db , workdir, flagCopy = True, flagClean=True, extDest=".fit", fitseq=False  ) :
        threading.Thread.__init__(self)
        extDest=".fit"
        log.clear()
        self.param=(log, db, workdir, flagCopy, flagClean, fitseq,extDest)

    # --------------------------------------------------------------------------
    def run(self):
        (log, db, workdir, flagCopy, flagClean, fitseq,extDest)=self.param
        task = GestionThread()
        task.SetRunning(self)
        
        # Calcul le nombre d'etape pour la barre de progression:
        StepCnt=0
        for keystr,dbImage in  db.GetDb().items():
            if db.Item_IsInitialised(keystr ) == "uninitialized" :
                continue
            for iodf in dbImage.db['Images'] :
                if iodf.IsInitialised() ==  "" :
                    continue
                StepCnt = StepCnt + 1
        log.SetStepNumber(StepCnt)
    
        # Construction de l'arborescence des dossiers
        StepCnt=0
        log.print_ligne("-")
        log.print_titre('-',_('Building directories'))
        for keystr,dbImage in  db.GetDb().items():
            objectname = dbImage.db['Object']
            layername  = dbImage.db['LayerName']
            sessionname= dbImage.db['Session']
            if db.Item_IsInitialised(keystr ) == "uninitialized" :
                log.insert(_("Warning")+ " " + keystr + " " + _("uninitialized") + " => " + _("skipping") +'\n')
                continue
            dir = os.path.join( workdir, objectname, layername, sessionname )
            for iodf in dbImage.db['Images'] :
                if task.IsAborted():
                    return
                if iodf.IsInitialised() ==  "" :
                    continue
                if iodf.IsLib():
                    continue
                data = iodf.GetData()
                subdir= os.path.join( dir, data['typename'] )
                if  not os.path.exists(subdir):
                    log.insert(_("Make directory:")+ " " + subdir +'\n' )
                    mkdirs(subdir)
                else: # le dossier existe, nettoyage si option active
                    if flagClean :
                        data = iodf.GetData()
                        xx,srcExt = os.path.splitext(data['files'][0])
                        nettoyage_dossier(log, subdir, srcExt )
                        singledir = os.path.join( subdir,"single" )
                        if os.path.exists(singledir) :
                            nettoyage_dossier(log, singledir, "*.*" )

    
            subdir= os.path.join( dir, MASTERDIR )
            if  not os.path.exists(subdir):
                log.insert(_("Make directory:")+ " " + subdir +'\n' )
                mkdirs(subdir)
                log.insert("\n")
            subdir= os.path.join( workdir, objectname,  TEMPDIR )
            if  not os.path.exists(subdir):
                log.insert(_("Make directory:")+ " " + subdir +'\n' )
                mkdirs(subdir)
                log.insert("\n")
    
        # Copie des fichiers
        overwriteFiles = flagClean
        log.print_ligne("-")
        log.print_titre('-',_('COPYING TO SIRIL DIRECTORY'))
        log.print_ligne("-")
        for keystr,dbImage in  db.GetDb().items():
            objectname = dbImage.db['Object']
            layername  = dbImage.db['LayerName']
            sessionname= dbImage.db['Session']
            if db.Item_IsInitialised(keystr ) == "uninitialized" :
                continue
            log.print_ligne("-")
            dir = os.path.join( workdir, objectname, layername, sessionname )
            for iodf in dbImage.db['Images'] :
                if iodf.IsInitialised() ==  "" :
                    continue
                StepCnt = StepCnt + 1
                log.SetProgressBar(StepCnt)
                data = iodf.GetData()
                if iodf.IsLib() :
                    if not data['copylib'] :
                        destfile= os.path.join( dir,  MASTERDIR,"master-"+data['typename'][:-1].lower() + extDest )
                        srcfile= data['files'][0]
                        if overwriteFiles == False :
                            cr =CheckOverwrite(log, destfile)
                            if cr == None :
                                pass
                            elif cr == False :
                                overwriteFiles = True
                            else:
                                return
                        if copy_link( log, flagCopy, srcfile, destfile ) :
                            return
                else:
                    counter=1
                    fmt="{0:05d}"
                    destdir= os.path.join( dir, data['typename'] )
                    suffixe = ""
                    if fitseq :
                        suffixe = "single_"
                        destdir= os.path.join(destdir,"single")
                        mkdirs(destdir)
                    prefixDestFile = os.path.join(destdir,suffixe + data['typename'].lower() )
                    srcfiles= iodf.GetFilesExpanded()
                    extDest2=extDest
                    flagCopy2=flagCopy
                    # Gestion des RAW APN
                    xx,srcExt = os.path.splitext(data['files'][0])
                    if isFitExt(srcExt) == False :
                        # on conserve l'extension dans le cas des raw
                        extDest2 =  srcExt.lower()
                        # Force la copie car les liens symboliques ne marche pas
                        # avec libraw sur windows
                        #if sys.platform.startswith('win32'):
                        #    flagCopy2 = True
    
                    srcfiles.sort(key=os.path.getctime)
                    for srcfile in srcfiles :
                        numero = fmt.format(counter)
                        destfile = prefixDestFile + numero + extDest2
                        if overwriteFiles == False :
                            cr =CheckOverwrite(log, destfile)
                            if cr == None :
                                pass
                            elif cr == False :
                                overwriteFiles = True
                            else:
                                return
                        if copy_link( log, flagCopy2, srcfile, destfile ) :
                            return
                        counter=counter+1
                    if isFitExt(srcExt) and (fitseq==False) :
                        generation_sefile(prefixDestFile + ".seq" ,data['typename'].lower(), counter-1 )
        log.insert('\n')
        log.print_ligne('.')
        log.print_titre('.',_("COPY : FINISHED"))
        log.print_ligne('.')
        log.insert('\n')
        log.SetFinishBar()
        
# ------------------------------------------------------------------------------
def copy_link(log,flagCopy,src,dst):
    task = GestionThread()
    if task.IsAborted():
        log.AbortMsg()
        return True

    if src == dst :
        log.insert(_('Skip') + ' ' + src + " == " + dst + '\n')
        return False

    if  flagCopy == True :
        TypeAction=_('Copy')
    else:
        TypeAction=_('Link')

    log.insert( TypeAction + ' ' + src + " -> " + dst + '\n')
    try:
        if os.path.exists(dst) :
            os.remove(dst)
        if flagCopy == True :
            shutil.copy2(src, dst )
        else:
            if sys.platform.startswith('win'):
                subprocess.check_call('mklink "%s" "%s"' % ( dst, src ), shell=True)
            else:
                os.symlink( src , dst )
    except Exception as e:
        log.insert(_("... aborted") +"\n")
        log.insert("*** copy_link() :" + str( e ) + '\n')        
        if sys.platform.startswith('win') and flagCopy == False :
            log.insert("***" + _("Symbolic Link Error on Windows: "))
            log.insert(_('Check if the "Developer Mode" option is enabled')+"\n")
        return True
    return False

# ------------------------------------------------------------------------------
def CheckOverwrite(log,file):
    cr=None
    if os.path.exists(file) :
        dlg = wx.MessageDialog(None,
                _( "Are you sure you want to overwrite image files?"), 'Error',
                 wx.NO|wx.YES | wx.ICON_ERROR)
        value=dlg.ShowModal()
        dlg.Destroy()
        if value == wx.ID_NO :
            log.insert(_("... aborted")+"\n")
            cr=True
        else:
            cr=False
    return cr

# ==============================================================================
def Copy_fusion_session(log, arbre , workdir, flagCopy = True, flagClean=True, extDest=".fit"  ) :
    for objectname,layers in  arbre.items() :
        srcfiles=[]
        for layername,sessions in layers.items():
            if len(sessions) == 1 :
                continue

            for session,values in sessions.items() :
                if values[1] is None :
                    continue
                for file in glob.glob(os.path.join(workdir,objectname,layername,session,TYPENAME[IMAGE], values[1] + "*" + extDest)) :
                        srcfiles.append( file )

            if len(srcfiles) == 0 :
                continue
            counter=1
            fmt="{0:05d}"
            imagename="image_group"
            groupdir= os.path.join(workdir,objectname,layername,GROUPDIR )
            mkdirs( groupdir )
            nettoyage_dossier(log, groupdir )
            prefixDestFile=os.path.join(groupdir,imagename)
            for srcfile in srcfiles :
                numero = fmt.format(counter)
                if copy_link( log, flagCopy, srcfile, prefixDestFile + numero + extDest) :
                    return True
                counter=counter+1
            generation_sefile(prefixDestFile + ".seq" , imagename, counter-1 )
            srcfiles=[]
    log.insert('\n')
    log.print_ligne('.')
    log.print_titre('.',_("MERGE : FINISHED"))
    log.print_ligne('.')
    log.insert('\n')
    return False

# ==============================================================================
def generation_sefile( fileseq , sequence_name, nb_images ) :
    with io.open(fileseq, 'w', newline='') as fd :
        fd.write('#Siril sequence file. Contains list of files (images), selection, and registration data\n')
        fd.write("#S 'sequence_name' start_index nb_images nb_selected fixed_len reference_image version\n")
        fd.write("S '" + sequence_name + "' 1 " + str(nb_images) + " " + str(nb_images) + " 5 -1 1\n")
        fd.write("L -1\n")
        for num in range(1, nb_images + 1):
            fd.write("I " + str(num) + " 1\n")

