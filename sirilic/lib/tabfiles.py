# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL script.
# It can also group scripts.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import wx
import os
import sirilic.ui.gui as gui
from sirilic.lib.constantes import *

# ==============================================================================
class FileDropTarget(wx.FileDropTarget):
    def __init__(self, target_obj):
        wx.FileDropTarget.__init__(self)
        self.target_obj = target_obj

    def OnDropFiles(self, x, y, filenames):
        for file in filenames :
            self.target_obj.Append(file )
        return True


# ==============================================================================
class TabFiles:
    def __init__(self, i_gui, i_prefs,ChangeTab ):

        self.i_files = i_gui.wfiles
        self.prefs   = i_prefs
        self.ChangeTab = ChangeTab

        self.i_files.lb_Img.SetDropTarget(   FileDropTarget(self.i_files.lb_Img))
        self.i_files.lb_Offset.SetDropTarget(FileDropTarget(self.i_files.lb_Offset))
        self.i_files.lb_Dark.SetDropTarget(  FileDropTarget(self.i_files.lb_Dark))
        self.i_files.lb_Flat.SetDropTarget(  FileDropTarget(self.i_files.lb_Flat))
        self.i_files.lb_DFlat.SetDropTarget( FileDropTarget(self.i_files.lb_DFlat))

        self.i_files.bAdd_Img.Bind(wx.EVT_BUTTON   , lambda evt, target_obj=self.i_files.lb_Img   : self.CB_GetFiles(evt, target_obj))
        self.i_files.bAdd_Offset.Bind(wx.EVT_BUTTON, lambda evt, target_obj=self.i_files.lb_Offset: self.CB_GetFiles(evt, target_obj))
        self.i_files.bAdd_Dark.Bind(wx.EVT_BUTTON  , lambda evt, target_obj=self.i_files.lb_Dark  : self.CB_GetFiles(evt, target_obj))
        self.i_files.bAdd_Flat.Bind(wx.EVT_BUTTON  , lambda evt, target_obj=self.i_files.lb_Flat  : self.CB_GetFiles(evt, target_obj))
        self.i_files.bAdd_DFlat.Bind(wx.EVT_BUTTON , lambda evt, target_obj=self.i_files.lb_DFlat : self.CB_GetFiles(evt, target_obj))

        self.i_files.bDEL_Img.Bind(wx.EVT_BUTTON   , lambda evt, target_obj=self.i_files.lb_Img   : self.CB_DelLine(evt, target_obj))
        self.i_files.bDEL_Offset.Bind(wx.EVT_BUTTON, lambda evt, target_obj=self.i_files.lb_Offset: self.CB_DelLine(evt, target_obj))
        self.i_files.bDEL_Dark.Bind(wx.EVT_BUTTON  , lambda evt, target_obj=self.i_files.lb_Dark  : self.CB_DelLine(evt, target_obj))
        self.i_files.bDEL_Flat.Bind(wx.EVT_BUTTON  , lambda evt, target_obj=self.i_files.lb_Flat  : self.CB_DelLine(evt, target_obj))
        self.i_files.bDEL_DFlat.Bind(wx.EVT_BUTTON , lambda evt, target_obj=self.i_files.lb_DFlat : self.CB_DelLine(evt, target_obj))

        self.i_files.bCLR_Img.Bind(wx.EVT_BUTTON   , lambda evt, target_obj=self.i_files.lb_Img   : self.CB_ClearAll(evt, target_obj))
        self.i_files.bCLR_Offset.Bind(wx.EVT_BUTTON, lambda evt, target_obj=self.i_files.lb_Offset: self.CB_ClearAll(evt, target_obj))
        self.i_files.bCLR_Dark.Bind(wx.EVT_BUTTON  , lambda evt, target_obj=self.i_files.lb_Dark  : self.CB_ClearAll(evt, target_obj))
        self.i_files.bCLR_Flat.Bind(wx.EVT_BUTTON  , lambda evt, target_obj=self.i_files.lb_Flat  : self.CB_ClearAll(evt, target_obj))
        self.i_files.bCLR_DFlat.Bind(wx.EVT_BUTTON , lambda evt, target_obj=self.i_files.lb_DFlat : self.CB_ClearAll(evt, target_obj))

        self.i_files.bEdit_Img.Bind(wx.EVT_BUTTON   , lambda evt, target_obj=self.i_files.lb_Img   : self.CB_EditLine(evt, target_obj))
        self.i_files.bEdit_Offset.Bind(wx.EVT_BUTTON, lambda evt, target_obj=self.i_files.lb_Offset: self.CB_EditLine(evt, target_obj))
        self.i_files.bEdit_Dark.Bind(wx.EVT_BUTTON  , lambda evt, target_obj=self.i_files.lb_Dark  : self.CB_EditLine(evt, target_obj))
        self.i_files.bEdit_Flat.Bind(wx.EVT_BUTTON  , lambda evt, target_obj=self.i_files.lb_Flat  : self.CB_EditLine(evt, target_obj))
        self.i_files.bEdit_DFlat.Bind(wx.EVT_BUTTON , lambda evt, target_obj=self.i_files.lb_DFlat : self.CB_EditLine(evt, target_obj))
        self.i_files.bClearAllFiles.Bind(wx.EVT_BUTTON ,self.ClrAllListBoxFiles )
        
        self.i_files.lb_Img.Bind(wx.EVT_CONTEXT_MENU   , lambda evt, target_obj=self.i_files.lb_Img   : self.CB_popupmenu(evt, target_obj))
        self.i_files.lb_Offset.Bind(wx.EVT_CONTEXT_MENU, lambda evt, target_obj=self.i_files.lb_Offset: self.CB_popupmenu(evt, target_obj))
        self.i_files.lb_Dark.Bind(wx.EVT_CONTEXT_MENU  , lambda evt, target_obj=self.i_files.lb_Dark  : self.CB_popupmenu(evt, target_obj))
        self.i_files.lb_Flat.Bind(wx.EVT_CONTEXT_MENU  , lambda evt, target_obj=self.i_files.lb_Flat  : self.CB_popupmenu(evt, target_obj))
        self.i_files.lb_DFlat.Bind(wx.EVT_CONTEXT_MENU , lambda evt, target_obj=self.i_files.lb_DFlat : self.CB_popupmenu(evt, target_obj))

    # --------------------------------------------------------------------------
    # Efface les listbox 
    def ClrAllListBoxFiles( self,event=None ) :
        self.i_files.lb_Img.Clear()
        self.i_files.lb_Offset.Clear()
        self.i_files.lb_Dark.Clear()
        self.i_files.lb_Flat.Clear()
        self.i_files.lb_DFlat.Clear()
    # --------------------------------------------------------------------------
    # charge les listes de fichiers dans les listbox 
    def Set(self,files):
        self.ClrAllListBoxFiles()
        # files
        self.setListBoxFiles(self.i_files.lb_Img   , files[IMAGE]    )
        self.setListBoxFiles(self.i_files.lb_Offset, files[OFFSET]   )
        self.setListBoxFiles(self.i_files.lb_Dark  , files[DARK]     )
        self.setListBoxFiles(self.i_files.lb_Flat  , files[FLAT]     )
        self.setListBoxFiles(self.i_files.lb_DFlat , files[DFLAT] )
        
    def setListBoxFiles( self, listbox, listfiles ) :
        if not listfiles  :
            return
        listbox.Set(listfiles)
    # --------------------------------------------------------------------------
    # recupere les listes de fichiers dans les listbox 
    def Get(self):
        files = [ None ] * NB_IODF
        files[IMAGE]  = self.getListBoxFiles( self.i_files.lb_Img    )
        files[OFFSET] = self.getListBoxFiles( self.i_files.lb_Offset )
        files[DARK]   = self.getListBoxFiles( self.i_files.lb_Dark   )
        files[FLAT]   = self.getListBoxFiles( self.i_files.lb_Flat   )
        files[DFLAT]  = self.getListBoxFiles( self.i_files.lb_DFlat  )
        return files 
        
    def getListBoxFiles( self, listbox ) :
        files =[]
        for item in range(listbox.GetCount()):
            file=listbox.GetString(item)
            if len(file) != 0 :
                files.append(file)
        return files
        
    # --------------------------------------------------------------------------
    # Callback generique pour ajouter un fichier a une lisbox 
    def CB_GetFiles(self, event, target_obj ):
        if target_obj.GetCount() == 0 :
            dir = self.prefs.Get('workdir')
        else:
            item = target_obj.GetSelection()
            if item == - 1 :
                item = 0
            dir=os.path.dirname(target_obj.GetString(item))
            if not os.path.exists(dir) :
                dir = self.prefs.Get('workdir')
        typefile="Fit files |*.fit;*.fts;*.fits|All file|*.*"
        flag    =wx.FD_OPEN | wx.FD_FILE_MUST_EXIST|wx.FD_MULTIPLE
        with wx.FileDialog(None, _("Open"), dir, "",typefile , flag) as openFileDialog :
            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                return
            for file in openFileDialog.GetPaths() :
                if len(file) != 0 :
                    target_obj.Append( file )
            self.ChangeTab(TAB_FILES)

    # --------------------------------------------------------------------------
    # Callback generique pour effacer une lisbox 
    def CB_ClearAll(self, event, target_obj ):
        target_obj.Clear()
        self.ChangeTab(TAB_FILES)

    # --------------------------------------------------------------------------
    # Callback generique detruire une lifne d'une lisbox 
    def CB_DelLine(self, event, target_obj ):
        try:
            target_obj.Delete(target_obj.GetSelection())
            self.ChangeTab(TAB_FILES)
        except:
            pass
    def CB_EditLine(self, event, target_obj ):
        item = target_obj.GetSelection()
        if item == -1 and target_obj.GetCount() != 0 :
            item = 0
        if item == -1 or target_obj.GetCount() == 0 :
            with gui.CModifyFile(None, -1, "") as dlg :
                dlg.text_file.SetValue("")
                if dlg.ShowModal() == wx.ID_CANCEL:
                    return
                file=dlg.text_file.GetValue()
                if len(file) != 0 :
                    target_obj.Append( file )
            return

        filename = target_obj.GetString(item)
        with gui.CModifyFile(None, -1, "") as dlg :
            dlg.text_file.SetValue(filename)
            if dlg.ShowModal() == wx.ID_CANCEL:
                return
            file = dlg.text_file.GetValue()
            if len(file) != 0 :
                target_obj.SetString(item, file )
            else:
                target_obj.Delete(item)
            self.ChangeTab(TAB_FILES)

