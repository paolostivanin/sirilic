# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL script.
# It can also group scripts.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import sys
import glob
import os
import io
import gettext

from sirilic.lib.tools import init_language
from sirilic.lib.tools import isFitFile

init_language()
from sirilic.lib.constantes import *

_ = gettext.gettext

# ==============================================================================

# global counter
StepCounter=0

# ==============================================================================        
class CSsfWriter:
    def __init__(self):
        self.fd = None        
   # --------------------------------------------------------------------------
    def write(self,  chaine ):
        try:
            if sys.platform.startswith('win32')  :
                # conversion des accents locale en utf8 pour le script
                #code=locale.getpreferredencoding(True)
                chaine=chaine.rstrip('\r')
                #bchaine = chaine.encode(errors='ignore')
                #chaine = bchaine.decode(code,errors='ignore')
            self.fd.write( chaine )
        except Exception as e :
            print("error CSsfWriter::write() : " + str(e) )
            
    def comment(self, text="",space=" " ):
        self.write("#"+space + text +"\n")
        
    def command(self, cmd="",arguments=" " ):
        self.write(cmd + " " + arguments +"\n")
    
    def hline(self, car='#',space='' ):
        cnt=80-1-len(space)       
        self.write("#"+ space + car*80 +"\n")
        
    def title(self, text,cr=0):    
        self.comment()
        self.comment(text)
        self.comment()
        self.cr(cr)
       
    def cr(self, cnt=1 ):
        if cnt >0:
            self.write("\n"*cnt)
            
# ==============================================================================        
class CBasicSirilScript(CSsfWriter):
    def __init__(self, log, sirilprefs , db_images, glob_prefs):
        self.log     = log
        self.prefs   = sirilprefs
        self.db      = db_images
        self.props   = glob_prefs
        
        self.extDest       = ".fit"       
        self.dev           = self.prefs.Get('siril_dev')       
        self.compatibility = COMPATIBILITY_SIRIL[self.dev]    
        self.avancement    = ""
    # --------------------------------------------------------------------------
         
    def chdir(self, dirname=".", cmnt="", cr=0 ):
        if len(cmnt)>0:
            self.comment(cmnt)
        self.write("cd '" + dirname.replace('\\','/') +"'\n")
        self.cr(cr)

    def updir(self,cr=0 ):
        self.chdir("..",cr=cr)

    def copy(self,dst,src ):
        self.command("load", "'" + src + "'" )
        self.command("save", dst )
        
    def copy_multiple(self,list_dst,src ):
        self.command("load", "'" + src + "'" )
        for dst in list_dst :
            self.command("save", dst )

    def savetif(self,imagename,cond=True ):
        if cond is True :
            self.command("savetif", imagename )            
    
    def init_progress(self, workdir,objectname,layername,session):
        self.avancement = os.path.join(objectname,layername,session)
        self.imagedir   = os.path.join(workdir,self.avancement)
        self.avancement = self.avancement + ": "

    def progress( self, step_cur, maj=True):
        global StepCounter
        StepCounter = StepCounter + 1
        step_new = self.avancement + step_cur
        self.write("#TAG#{ ["+str(StepCounter)+ "] " + step_new + " ...\n")
        if maj :
            self.avancement = step_new  + ", "
    
    def end_progress(self):
        global StepCounter
        self.write("#\n#TAG#} ["+str(StepCounter)+ "]\n\n")
    
    def getStepNumber(self):
        global StepCounter
        return StepCounter
    
    def ClearStepNumber(self):
        global StepCounter
        StepCounter=0
    
    def isBayerMatrix(self, layername ):
        return (layername == DSLR) or (layername == RGB) or (layername == "cHa") or (layername == "cHaO3")
    
    def isDeBayerised(self, layername ):
        return ( (layername == "dL") or (layername == "dR") or (layername == "dG") or (layername == "dB") or
                 (layername == "dHa") or (layername == "dHb") or (layername == "dS2") or (layername == "dO3")  )  
    
    def isMono(self, layername ):
        return ( (layername == "L") or (layername == "R") or (layername == "G") or (layername == "B") or
                (layername == "Ha") or (layername == "Hb") or (layername == "S2") or (layername == "O3")  )
    
    def isDSLR_Raw(self, layername):
        cond = (layername == DSLR) or (layername == "cHa") or (layername == "cHaO3") or self.isDeBayerised(layername )
        return cond
    
    # --------------------------------------------------------------------------
    def header_file(self ,nom_script):
        with io.open(nom_script, "w", newline='', encoding='utf-8') as self.fd :
            self.hline('#')
            self.title( _("Don't edit : generated by ") + sys.argv[0] )
            self.title(_("Compatibility with") + " Siril: Version >= " + self.compatibility )

            if self.dev :
                self.command('requires ' + self.compatibility )
                self.cr()
                if self.prefs.Get('float32b') :
                    self.command('set32bits')
                else:
                    self.command('set16bits')
                self.cr()
                    
                if self.prefs.Get('compress') :
                    hscale="" 
                    if self.prefs.Get('compress_type') == "hcompress":
                        hscale += " " + str(self.prefs.Get('compress_hscale'))
                    self.command( 'setcompress', '1 -type=' + self.prefs.Get('compress_type') + " " + str(self.prefs.Get('compress_quantif')) + hscale )
                else:
                    self.command('setcompress', '0')
                self.cr()
                
            self.command('setext',  self.extDest[1:] )
            self.cr()
            
            if self.prefs.Get('nbcpu') != 0 :
                self.command('setcpu' , str(self.prefs.Get('nbcpu')) )
                self.cr()
            if self.prefs.Get('nbmem') != 0 :
                self.command('setmem' , str(self.prefs.Get('nbmem')) )
                self.cr()

    # --------------------------------------------------------------------------
    def mk_stack_param( self, iodf ) :
        if not iodf["files"]:
            return ""  
        stack_param=" " + iodf['Stack'] + " "
        if iodf['Stack'] == "rej":
            stack_param= stack_param + str(iodf['RejL']) + " " + str(iodf['RejH']) + " "
    
        if iodf['Norm'] == "no":
            stack_param= stack_param + "-nonorm "
        else:
            stack_param= stack_param + "-norm=" + iodf['Norm'] + " "
    
        if iodf['type'] == IMAGE :
            if self.check_param_filt( iodf['fwhm'],20.0) :
                stack_param= stack_param + "-filter-fwhm=" + iodf['fwhm'] + " "
    
            # disable for Siril 0.9.12
            if self.prefs.Get("siril_dev") :
                if self.check_param_filt( iodf['wfwhm'],20.0) :
                    stack_param= stack_param + "-filter-wfwhm=" + iodf['wfwhm'] + " "
    
            if self.check_param_filt( iodf['round'],1.0 ) :
                stack_param= stack_param + "-filter-round=" + iodf['round'] + " "
    
            if  self.check_param_filt( iodf['quality'], 100.0 ) :
                stack_param= stack_param + "-filter-quality=" + iodf['quality'] + " "
    
        return stack_param

    # --------------------------------------------------------------------------
    def check_param_filt( self, value,  val_max ) :
        if value == None :
            return False
        if value == "0%" :
            return False
        if value == "100%" :
            return False
        if value == "" :
            return False
    
        if value[-1] == "%" :
            if self.isFloat( value[:-1] ) == False :
                return False
            nombre = float(value[:-1])
            if  nombre > 100 :
                return False
            if nombre <   0 :
                return False
        else:
            if self.isFloat( value[:-1] ) == False :
                return False
            nombre = float(value[:-1])
            if nombre <   0 :
                return False
            if nombre >   val_max :
                return False
    
        return True
    # --------------------------------------------------------------------------
    def isFloat(self, value ) :
        try:
            float(value)
            return True
        except ValueError:
            return False
        
    # --------------------------------------------------------------------------
    def SetFindStar(self, image ) :
        if image['Findstar'] :
            self.progress( "SetFindStar",maj=False)
            self.command("setfindstar", " " + str(image['Ksigma']) + " " + str(image['Roundness']) )
            self.end_progress( )
            
    # --------------------------------------------------------------------------
    def preprocess( self, cond, pp, output, offset=None,dark=None,flat=None , options=""):
        if cond :
            masters=""
            if offset is not None :
                masters=masters + " -bias=" + offset
            if dark is not None :
                masters=masters + " -dark=" + dark
            if flat is not None :
                masters=masters + " -flat=" + flat
            if (masters != "") :
                self.command("preprocess", output.lower() + masters + options )
                pp="pp_" + pp
        return pp
  
    # --------------------------------------------------------------------------
    def stacking( self, pp, imagename, out_imagename, stack_param ):
        self.command("stack", pp + imagename.lower() + stack_param + " -out=" + out_imagename)

# ==============================================================================        
class CScriptBuilder(CBasicSirilScript):
    def __init__(self, log, nom_script, sirilprefs , db_images, glob_prefs ):
        CBasicSirilScript.__init__(self,log, sirilprefs , db_images, glob_prefs)
        
        base,ext     = os.path.splitext(nom_script)
        self.script1 = base + "-part1" + ext
        self.script2 = base + "-part2" + ext
      
    # --------------------------------------------------------------------------
    def Build(self):
        self.ClearStepNumber()
        self.last_processed_image = ""
        self.BuildScriptPart1()
        self.BuildScriptPart2()
        return self.arbre, self.script1, self.script2, self.last_processed_image

    # --------------------------------------------------------------------------
    def BuildScriptPart1(self):  
        self.header_file(self.script1)
    
        liste_keystr = self.db.GetKeyStrSorted()
        self.arbre   = self.db.GetArborescence()
        for keystr in  liste_keystr :
            if self.db.Item_IsInitialised(keystr ) == "uninitialized" :
                self.log.insert(_("Warning")+ " " + keystr + " " + _("uninitialized") + " => " + _("skipping") +'\n')
                continue
            
            script_img = CBuildImage(self.log, self.prefs , self.db, self.props,keystr)
            
            outputs = script_img.Build(self.script1)
            if outputs is not None:
                for output in outputs:
                    self.last_processed_image = output[1]
                    xx,yy,zz = self.db.Split(keystr)
                    self.arbre[xx][yy][zz].append( output[0] )
    
        # fin de script
        if self.prefs.Get('workdirreturn') :
            with io.open(self.script1, "a", newline='', encoding='utf8' ) as self.fd :
                self.chdir( self.prefs.Get('workdir'),_("Work Directory") )
                self.cr()
    # --------------------------------------------------------------------------
    def BuildScriptPart2(self):      
        multi = self.props['multisession']
        
        script_multi = CMultisessionScript(self.log, self.prefs , self.db, self.props)
        last_processed_image_multi = script_multi.Build(self.script2,self.arbre)
            
        if  last_processed_image_multi != None :
            self.last_processed_image = last_processed_image_multi

# ==============================================================================        
# CONSTRUCTION DU SCRIPT  DE PRETRAITEMENT DE L'IMAGE (DARK/OFFSET/FLAT)
class CBuildImage(CBasicSirilScript):
    def __init__(self, log, sirilprefs , db_images, glob_prefs,keystr ):
        CBasicSirilScript.__init__(self,log, sirilprefs , db_images, glob_prefs)

        self.layer_is_multi = self.db.IsMultiSession(keystr)
        obj_iodf            = self.db.GetItem(keystr)
        self.objectname,self.layername,self.session = keystr.split("/")
        
        self.offset    = obj_iodf.GetDbIodf(OFFSET)
        self.dark      = obj_iodf.GetDbIodf(DARK)
        self.darkflat  = obj_iodf.GetDbIodf(DFLAT)
        self.flat      = obj_iodf.GetDbIodf(FLAT)
        self.image     = obj_iodf.GetDbIodf(IMAGE)
        
        self.bfitseq   = self.prefs.Get('fitseq')
        self.bSavetif  = self.prefs.Get('savetif')
        self.workdir   = self.prefs.Get('workdir')
        self.multi     = self.props['multisession']
        
        self.ena_stack = (    (self.multi == False               ) 
                           or (self.props['stack_intermed']==True)
                           or (self.layer_is_multi == False      )    )
   
        self.ena=[False]*NB_IODF
        self.enaLib=[False]*NB_IODF
        for ii in range(NB_IODF) :
            self.ena[ii]    = (obj_iodf.GetIodf(ii).IsInitialised() != '')
            self.enaLib[ii] =  obj_iodf.GetIodf(ii).IsLib()
    
        # PRECALCUL DES VARIABLES
        dir_master      = "../" + MASTERDIR
        self.master_offset   = dir_master + "/master-" + self.offset['typename'][:-1].lower()   + self.extDest
        self.master_dark     = dir_master + "/master-" + self.dark['typename'][:-1].lower()     + self.extDest
        self.master_darkflat = dir_master + "/master-" + self.darkflat['typename'][:-1].lower() + self.extDest
        self.master_flat     = dir_master + "/master-" + self.flat['typename'][:-1].lower()     + self.extDest
    
        self.seuils_cosme=" " + str(self.image['Cold']) + " " + str(self.image['Hot']) + " "
    
        self.drizzle=""
        if self.image['Drizzle'] :
            self.drizzle=" -drizzle "
    
        self.flip=""
        if self.image['Flip']  :
            self.flip=" -flip "
    
        # disable for Siril > 0.99
        self.stretch=""
        if (not self.dev ) and (self.props['stretch'] ) :
            stretch=" -stretch "
    
        cfaequa=""
        if self.props['CFAequa'] :
            cfaequa=" -equalize_cfa "
    
        self.cfa=""
        if (self.layername == DSLR) or (self.layername == RGB) :
            self.cfa=" -cfa " + cfaequa
            if not self.image['Cosmetic'] :
                self.cfa= self.cfa + " -debayer " + self.stretch + self.flip
        if (self.layername == "cHa") or (self.layername == "cHaO3")  :
            self.cfa=" -cfa " + cfaequa
        self.dark_opt=""
        if self.image['DarkOpt'] :
            self.dark_opt=" -opt "
        
        self.init_progress(self.workdir,self.objectname,self.layername,self.session)
        
    # --------------------------------------------------------------------------
    # AJOUT DANS LE SCRIPT LE PRETRAITEMENT POUR UNE IMAGE
    def Build(self, nom_script): 
        out=""
        with io.open(nom_script, "a", newline='', encoding='utf-8' ) as self.fd :
            self.header()           
            self.copylib_already_processed()    
            self.offset_processing()
            self.dark_processing()
            self.darkflat_processing()
            self.flat_processing()
            out = self.light_processing()
        return out
    
    # --------------------------------------------------------------------------
    def header(self):
        self.hline('#')
        self.cr()
        self.title( _('Automating: layer ')  + self.layername + _(' of ') + self.session )
        self.cr()
        self.chdir( self.imagedir, _("Work Directory"), 1 )

    # --------------------------------------------------------------------------
    def copylib_already_processed(self): # COPIE DES LIBRAIRIES : Resultat d'un script precedent
        cond = self.offset['copylib'] and self.enaLib[OFFSET] and self.ena[OFFSET]
        self.copy_lib( cond, self.offset )

        cond = self.dark['copylib'] and self.enaLib[DARK] and self.ena[DARK]
        self.copy_lib( cond, self.dark )

        cond = self.flat['copylib'] and self.enaLib[FLAT] and self.ena[FLAT]
        self.copy_lib( cond, self.flat )

        cond = self.darkflat['copylib'] and self.enaLib[DFLAT] and self.ena[DFLAT]
        self.copy_lib( cond, self.darkflat )
          
    # --------------------------------------------------------------------------
    def offset_processing(self):             
        cond = (not self.enaLib[OFFSET] ) and self.ena[OFFSET]
        if cond == False :
            return 

        self.preambule( self.offset)
        
        stack_param = self.mk_stack_param( self.offset  )
        self.stacking( "", self.offset['typename'], self.master_offset, stack_param )
        
        self.postambule( )
            
    # --------------------------------------------------------------------------
    def dark_processing(self):
        cond = (not self.enaLib[DARK] ) and self.ena[DARK] 
        if cond == False :
            return
        
        self.preambule( self.dark )

        pp=self.preprocess( self.ena[OFFSET] and self.dark['suboffset'], "",  self.dark['typename'], offset=self.master_offset )
        
        stack_param = self.mk_stack_param( self.dark  )
        self.stacking( pp, self.dark['typename'], self.master_dark, stack_param )
        
        self.postambule( )
           
    # --------------------------------------------------------------------------
    def darkflat_processing(self):
        cond = (not self.enaLib[DFLAT] ) and self.ena[DFLAT]
        if cond == False :
            return
        
        self.preambule( self.darkflat)

        pp=self.preprocess( self.ena[OFFSET] and self.darkflat['suboffset'], "",
                            self.darkflat['typename'], offset=self.master_offset )
        
        stack_param = self.mk_stack_param( self.darkflat  )
        self.stacking( pp, self.darkflat['typename'], self.master_darkflat, stack_param )
        
        self.postambule( )
         
    # --------------------------------------------------------------------------
    def flat_processing(self):
        cond = (not self.enaLib[FLAT] ) and self.ena[FLAT] 
        if cond == False :
            return
        
        self.preambule( self.flat)
        
        master_offset   = None
        master_darkflat = None
        cond = False
        if self.ena[OFFSET] and self.flat['suboffset'] :
            master_offset = self.master_offset
            cond = True
        if self.ena[DFLAT] :
            master_darkflat = self.master_darkflat
            cond = True
        pp=self.preprocess( cond , "",  self.flat['typename'], offset=master_offset, dark=master_darkflat  )
        
        stack_param = self.mk_stack_param( self.flat  )
        self.stacking( pp, self.flat['typename'], self.master_flat, stack_param )
        
        self.postambule( )
 
    # --------------------------------------------------------------------------
    def light_processing(self):
        cond = ( not self.enaLib[IMAGE] ) and self.ena[IMAGE]  
        if cond == False :
            return None
        
        self.preambule( self.image, _("PROCESSING"))
        
        pp=""
        master_offset = None
        master_dark   = None
        master_flat   = None
        cond = (self.cfa != "" )
        if self.ena[OFFSET] and self.image['suboffset'] :
            master_offset = self.master_offset
            cond = True
        if self.ena[DARK] :
            master_dark = self.master_dark
            cond = True
        if self.ena[FLAT] :
            master_flat = self.master_flat
            cond = True

        pp=self.preprocess( cond, pp,  self.image['typename'],
                           offset=master_offset, dark=master_dark, flat=master_flat,
                           options= self.cfa + self.dark_opt)

        pp=self.cosmetic( pp )
        
        self.postambule( )

        output=[]
        self.last_processed_image=""
        if self.ena_stack :
            self.SetFindStar(self.image )
                
            if self.dev :
                typename=self.image['typename'].lower()
                if (self.layername == "cHa") : 
                    self.progress( "extract Ha", maj=False)
                    self.chdir( self.image['typename'] )
                    self.command("seqextract_Ha", pp + typename )
                    self.updir()
                    pp="Ha_" + pp
                    self.drizzle=" -drizzle "
                    self.end_progress( )
                if (self.layername == "cHaO3")  : 
                    self.progress( "extract Ha-Oiii", maj=False)
                    self.chdir( self.image['typename'] )
                    self.command("seqextract_HaOIII", pp + typename )
                    self.updir()
                    self.drizzle=" -drizzle "
                    self.end_progress( )
                    self.pp1,self.last_processed_image= self.register_stack(  "Ha_" + pp )
                    if not self.multi :
                        self.pp1 = None
                    output.append([self.pp1,self.last_processed_image + self.extDest])
                    pp="OIII_" + pp
            pp,self.last_processed_image = self.register_stack( pp )
         
        self.command("close")
        self.cr()
        if not self.multi :
            pp = None
        last_processed_ext=None
        if self.last_processed_image != None :
            last_processed_ext=self.last_processed_image + self.extDest
        output.append([pp,last_processed_ext])
        return output
        
    # --------------------------------------------------------------------------
    def preambule(self, iodf, msg = _("MASTER BUILDING") ):
        name=iodf['typename'][:-1]
        self.progress( name.lower())
        self.title(name + " " + msg )
        self.chdir(iodf['typename'])         
        self.convert_fit( iodf['typename'], iodf['files'] )

    # --------------------------------------------------------------------------
    def postambule(self):
        self.updir()
        self.end_progress( )
        
    # --------------------------------------------------------------------------
    def register_stack( self, pp):  
        last_processed_image=None
        layername=self.layername         
        self.progress( "register")
        self.chdir( self.image['typename'] )
        typename=self.image['typename'].lower()
        self.command("register", pp + typename + self.drizzle )
        noLayer = NoLayer[layername]
        if (layername == "cHaO3" ) :
            prefix=pp[0:2]
            if (prefix=='Ha') :
                layername="cHa"
            else:
                noLayer-=1
                layername="cO3"
        self.updir()
        self.end_progress( )
        
        self.progress( "stack")
        self.chdir( self.image['typename'] )
        stack_param = self.mk_stack_param( self.image   )        
        self.command("stack", "r_" + pp + typename + stack_param + " -out=../" + layername )
        self.comment(_("Copy") + " " + layername + " " + _("to") +" C%02d" % (noLayer,))
        basedir="../../../"
        if self.layer_is_multi is True :           
            session= "-" + self.session # evite les ecrasements
        else:
            session= ""
            last_processed_image= os.path.join(self.workdir, self.objectname,  self.objectname + "_" + layername  )

        names=( basedir + TEMPDIR + "/C%02d" % (noLayer,) +  session,
                basedir + self.objectname + "_"  + layername + session )
        self.copy_multiple(src="../" + layername,list_dst=names)
        self.savetif( basedir + self.objectname + "_"  + layername +  session, self.bSavetif )
        self.updir()
        self.end_progress( )
        
        return pp,last_processed_image
         
    # --------------------------------------------------------------------------
    def copy_lib( self, cond, lib ):
        if cond :
            libname  = lib['files'][0].replace('\\','/')
            typename = lib['typename'][:-1]
            mastername = "master-" +  typename.lower() + self.extDest
            
            self.comment(_("COPY THE")+ " " + typename + " "
                         + _("LIBRARY INTO THE MASTER DIRECTORY"))
            self.chdir(MASTERDIR )
            self.copy(src=libname, dst=mastername)
            self.updir()

    # --------------------------------------------------------------------------
    # Conversion des images en fit
    def convert_fit( self, imagename , srcpath ):
        # A) Conversion des images autres que fit ou raw
        try:
            os.chdir( os.path.join(self.imagedir, imagename ) )
            list_file = glob.glob(  imagename.lower() +"*" )
        except Exception as e :
            self.log.insert(_("Warning")+ " : " + _("Abort Convert to fit: skip this step")+"\n")
            self.log.insert("*** convert_fit() error: " + str( e ) + '\n')
            list_file = []
        
        ext=""
        for ff in list_file:
            xx,ext = os.path.splitext(ff)
            ext=ext.lower()
            if ext != ".seq" :
                break
            
        if ( ext==".tif" or ext==".tiff" or ext==".bmp" or ext==".png"
             or ext==".jpg"  or ext==".jpeg"  or ext==".pic" ) :          
            self.comment(_("CONVERSION:") + " " + imagename + " "
                         + _("TO FIT FORMAT"))
            for ff in list_file:
                xx,ext = os.path.splitext(ff)
                if ext != ".seq" :
                    self.copy(src=ff,dst=xx)                    
        
        if self.bfitseq : # B) Conversion des images en 1seul fichier fit
            self.command("cd single")
            self.command("convert", imagename.lower() + " -fitseq -out=.." )
            self.command("cd ..")
            #self.copy(src="s_" + imagename.lower(), dst=imagename.lower())
        else: # C) Conversion des images raw en fit
            if self.isDSLR_Raw(self.layername) and (isFitFile(srcpath[0]) == False) :
                self.command("convertraw",imagename.lower())           
 
    # --------------------------------------------------------------------------
    def cosmetic(self, pp ):
        if self.image['Cosmetic'] :
            typename=self.image['typename'].lower()
            if self.isBayerMatrix(self.layername) :
                self.command("seqfind_cosme_cfa",  pp + typename + self.seuils_cosme )
                self.command("preprocess",  "cc_"+ pp + typename + " -debayer " + self.stretch + self.flip )
                pp = "pp_cc_" + pp
            else:
                self.command("seqfind_cosme", pp + typename + self.seuils_cosme)
                pp = "cc_" + pp
        return pp
    
# ==============================================================================
# CONSTRUCTION DU SCRIPT MULTISESSION D'UNE IMAGE ET ALIGNEMENT DES COUCHES COULEUR
class CMultisessionScript(CBasicSirilScript): 
    def __init__(self, log, sirilprefs , db_images, glob_prefs ):
        CBasicSirilScript.__init__(self,log, sirilprefs , db_images, glob_prefs)
        
    # --------------------------------------------------------------------------   
    def Build(self, nom_script,arbre): 
        workdir      = self.prefs.Get('workdir')
        retworkdir   = self.prefs.Get('workdirreturn')
        bSavetif     = self.prefs.Get('savetif')
        multisession = self.props['multisession']
        
        last_processed_image=None
        self.header_file(nom_script)

        with io.open(nom_script, "a", newline='', encoding='utf-8') as self.fd : 
            for objectname,layers in  arbre.items() :
                ccd_mono = 0
                no_align = False
                for layername,sessions in layers.items():
                    if not ((layername==DSLR) or (layername==RGB) ) :
                        ccd_mono = ccd_mono +1
                    if (len(sessions) != 1) and ( multisession is False ):
                        no_align = True
                    if (len(sessions) == 1) or ( multisession is False ):
                        continue
                    for session,values in sessions.items() :
                        keystr   = values[0]
                        obj_iodf = self.db.GetItem(keystr)
                        image    = obj_iodf.GetDbIodf(IMAGE)
                        break
    
                    stack_param = self.mk_stack_param( image )
                    seuils_cosme=" " + str(image['Cold']) + " " + str(image['Hot']) + " "
                    drizzle=""
                    if image['Drizzle'] :
                        drizzle=" -drizzle "
                        
                    imagename="image_group"
                    
                    self.init_progress(workdir,objectname,layername,GROUPDIR)
                    self.chdir(self.imagedir, _("Image Directory"),cr=1)
                    
                    self.SetFindStar(image )

                    pp       = ''
                    no_duo   = 0
                    duo_name = ''
                    if self.dev :
                        if (layername == "cHa") : 
                            self.progress( "extract Ha", maj=False)
                            self.command("seqextract_Ha", imagename )
                            imagename="Ha_" + imagename
                            drizzle=" -drizzle "
                            self.end_progress( )
                        if (layername == "cHaO3")  : 
                            self.progress( "extract Ha-Oiii", maj=False)
                            self.command("seqextract_HaOIII", imagename )
                            drizzle=" -drizzle "
                            self.end_progress( )
                            self.register_stack_grp(objectname,layername,"Ha_" + imagename, drizzle, stack_param , 1 , "_Ha" )
                            pp="OIII_"
                            duo_name="_O3"
                    last_processed_image = self.register_stack_grp(objectname,layername,pp + imagename, drizzle, stack_param , 0, duo_name )
    
                imagedir=os.path.join(workdir,objectname)
                self.avancement = objectname + " : "
                if (ccd_mono > 1) and (no_align is False) :
                    self.chdir(imagedir+ "/" + TEMPDIR,_("Object Directory"),cr=1)
                    self.comment(_("Register CCD Layers"))
                    
                    self.progress("Register CCD Layers")
                    self.command("register", "C")
                    for layername in layers.keys():
                        last_processed_image="final_" + objectname + "_" + layername
                        noLayer = NoLayer[layername]
                        self.copy(src="r_C%02d" % (noLayer,), dst="../" + last_processed_image )
                        self.savetif("../"+ last_processed_image,bSavetif)
                        last_processed_image = os.path.join(imagedir, last_processed_image  + self.extDest)
                    self.end_progress( )
                if retworkdir :
                    self.chdir( workdir,_("Work Directory"),cr=1)
                else:
                    self.chdir( imagedir,_("Object Directory"),cr=1)
    
            self.avancement = "... "
            self.progress( _("Finished") + " " )
            self.end_progress( )
            if last_processed_image is not None :
                last_processed_image=last_processed_image.replace('\\','/')
                self.comment(_("Last processed image") + ":" + last_processed_image)

            return last_processed_image
        
    # --------------------------------------------------------------------------   
    def register_stack_grp(self, objectname, layername,imagename, drizzle, stack_param , no_duo, duo_name ):
        self.progress( "register" + duo_name)
        self.command("register", imagename + drizzle )
        self.end_progress( )
    
        self.progress( "stack" + duo_name)
        noLayer = NoLayer[layername] - no_duo
        
        self.command("stack", "r_" + imagename + stack_param + " -out=../" + layername + duo_name )
        self.comment(_("Copy") + " " + layername + duo_name + " " + _("to") +" C%02d" % (noLayer,) )
        basedir="../../"
        names=( basedir + objectname + "_" + layername + duo_name ,
                basedir + TEMPDIR + "/C%02d" % (noLayer,) )
        self.copy_multiple(src="../" + layername + duo_name, list_dst=names)
        last_processed_image = os.path.join(self.imagedir, names[0]  + self.extDest)
        self.command("close")
        return last_processed_image
    
        
