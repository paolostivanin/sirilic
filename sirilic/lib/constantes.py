# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL script.
# It can also group scripts.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import gettext
_ = gettext.gettext

__all__ = ["DSLR", "RGB",
           "TAB_PROCESSUS", "TAB_FILES", "TAB_LOG",
           "OFFSET", "DARK", "DFLAT", "FLAT", "IMAGE", "NB_IODF",
           "IODF_SORTED", "TYPENAME",
           "BGCOLOR1", "FGCOLOR1", "WBORDER",
           "MASTERDIR","GROUPDIR", "TEMPDIR",
           "NoLayer", "LAYERS",
           "COMPATIBILITY_SIRIL"
            ]


OFFSET  =0
DARK    =1
DFLAT   =2
FLAT    =3
IMAGE   =4
NB_IODF =5

TAB_PROCESSUS=0
TAB_FILES    =1
TAB_LOG      =3


MASTERDIR="MASTER"
GROUPDIR="GROUP"
TEMPDIR="Temp"

IODF_SORTED = [IMAGE, OFFSET, DARK, FLAT, DFLAT]
TYPENAME = [ "OFFSETS","DARKS", "DARKFLATS" , "FLATS",  "IMAGES" ]

BGCOLOR1="#708090" # SlateGray
FGCOLOR1="#FFFAFA" # Snow
WBORDER=2

DSLR="DSLR"
RGB="RGB"

NoLayer = {  RGB : -1, DSLR : -2, "cHa": -3, "cHaO3": -4, #-5 reserved cO3
            "L" : 0,"R" : 1, "G" : 2, "B" : 3,
            "Ha": 4,"O3": 5, "S2": 6, "Hb": 7,         
            "dL" :  8, "dR" : 9, "dG" : 10, "dB"  : 11,
            "dHa": 12, "dO3":13, "dS2": 14, "dHb" : 15
           }
LAYERS  = { _("DSLR"):DSLR, _("Color CCD"):RGB,
            _("Luminance"):"L" , _("Red"):"R", _("Green"):"G", _("Blue"):"B",
            _("H alpha"):"Ha", _("Oiii"):"O3", _("Sii"):"S2", _("H beta"):"Hb",
            _("H alpha [color]"):"cHa",_("H alpha / Oiii [color]"):"cHaO3",
            _("Luminance [debayer]"):"dL" , _("Red [debayer]"):"dR",
            _("Green [debayer]"):"dG", _("Blue [debayer]"):"dB",
            _("H alpha [debayer]"):"dHa", _("Oiii [debayer]"):"dO3",
            _("Sii [debayer]"):"dS2", _("H beta [debayer]"):"dHb",
            }

COMPATIBILITY_SIRIL= ("0.9.12.0", "0.99.4.0", "0.99.6.0")
