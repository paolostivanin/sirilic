import sys
import os
import glob
import shutil

path_exec=os.path.dirname(  __file__ )
if not path_exec :
    path_exec='.'
sys.path.append(path_exec + os.sep + 'sirilic' + os.sep + 'lib')
import changelog

# ------------------------------------------------------------------------------
# clean build directories and files
def clean_build(dirlist=None,filelist=None):
    if dirlist is not None :
        for  dirglb in dirlist :
             for  dir in glob.glob( dirglb ):
                print("* clean:",dir )
                shutil.rmtree(dir,ignore_errors=True)
    if filelist is not None :
        for  fileglb in filelist :
            for  fic in glob.glob( fileglb ) :
                print("* clean:",fic )
                try:
                    os.unlink(fic)
                except:
                    pass
# ------------------------------------------------------------------------------      
def package_setuptools():
    from setuptools import setup, find_packages
    with open("README.md", "r") as fh:
        long_description = fh.read()

    setup(
        name = "sirilic",
        version = changelog.NO_VERSION,
        author="M27trognondepomme",
        author_email="M27trognondepomme@wordpress.com",
        license='LGPL-v3',
        description='SiriLic is a graphical frontend for SiriL scripting.',
        long_description=long_description,
        long_description_content_type="text/markdown",
        url="https://gitlab.com/free-astro/sirilic.git",
        packages=find_packages(),
        python_requires='>=3.6',
        classifiers=[
            "Programming Language :: Python :: 3",
            "Development Status :: 5 - Production/Stable",
            "Programming Language :: Python :: 3.6",
            "Topic :: Scientific/Engineering :: Astronomy",
            "License :: OSI Approved :: GNU General Public License v3 (GPLv3)"
        ],
        install_requires=[ 'wxPython' ],
        entry_points={
              #'console_scripts': [ 'sirilic=sirilic.App:Run', ],
              'gui_scripts': [ 'sirilic=sirilic.App:Run', ],
          },
        data_files=[('share/pixmaps',      ["sirilic/icon/sirilic.png"]),
                    ('share/applications', ["sirilic/ui/sirilic.desktop"]),
                     ],
        include_package_data=True
    )
# ------------------------------------------------------------------------------
def package_PyInstaller():
    import PyInstaller.__main__
    psrc=os.path.join(".","sirilic")
    sep=":"
    if sys.platform.startswith('win32'):
        sep=";"   
        
    cfg=[
        '--name=%s-V%s' % ('sirilic', changelog.NO_VERSION) ,
        '--noconfirm',
        '--onefile',  
        '--paths=%s'         % psrc,
        '--paths=%s'         % os.path.join( psrc, 'ui' ),
        '--paths=%s'         % os.path.join( psrc, 'lib' ),
        '--add-data=%s%s%s/' % (os.path.join( psrc, 'i18n'),sep,'i18n'),
        '--add-data=%s%s%s/' % (os.path.join( psrc, 'icon'),sep,'icon'),
        '--add-data=%s%s%s'  % (os.path.join( psrc, 'icon',"cp-nb.gif"),sep,'.'),
        '--add-data=%s%s%s'  % (os.path.join( psrc, 'icon',"cp-neg.gif"),sep,'.'),
    ]

    if sys.platform.startswith('win32'):
        cfg.append('--windowed')
        cfg.append('--icon=%s' % os.path.join( psrc, 'icon', 'cp-neg.ico') )

    if not sys.platform.startswith('win32'):
        cfg.append("--strip")

    cfg.append( os.path.join( psrc, 'Sirilic.pyw' ) )
    PyInstaller.__main__.run(cfg)
    
# ------------------------------------------------------------------------------
if len(sys.argv ) == 1 :
    print( "Build Package:" )
    print( "    o exec package :" )
    print( "        > python setup.py exec" )
    print( "")
    print( "    o python source package :" )
    print( "        > python setup.py sdist" )
    print( "")
    print( "    o python (*.whl) package :" )
    print( "        > python setup.py bdist_wheel " )
    print( "")
    print( "    o debian (*.deb) package :" )
    print( "        > python setup.py bdist_deb" )
    print( "")
    exit( 0 )
    
clean_build( [ 'build','dist','deb_dist', '*egg-info' ], ['*tar.gz','sirilic*.spec' ] )

if sys.argv[1] == "exec" :
    package_PyInstaller()
    clean_build( [ 'build'], ['sirilic*.spec' ] )
else:
    package_setuptools()
    clean_build( [ 'build', '*egg-info' ], ['*tar.gz' ] )
    
