# README (EN)

## SIRILIC

**SiriLic** ( **SiriL** **I**mage **C**onverter) is a software for preparing 
acquisition files (raw, Offset, Flat and Dark) for processing with SiriL software.

It does three things:

1. Structuring the SiriL working directory into sub-folders
2. Convert Raw, Offset, Dark or Flat files into SiriL sequence
3. Automatically generate the SiriL script according to the files present and the options

Sirilic allows also to batch process multiple channel and sessions. 

# LISEZ-MOI (FR)

## SIRILIC

**SiriLic** ( **Siril** **I**mage **C**onverter) est un logiciel de préparation 
des fichiers d’acquisition (brutes, Offset, Flat et Dark) pour les traiter avec 
le logiciel SiriL.

Il fait  3 choses :

1. Structuration du répertoire de travail SiriL en sous-dossier
2. Convertir les fichiers Brute, Offset, Dark ou Flat en séquence SiriL
3. Générer automatiquement le script SiriL en fonction des fichiers présents et des options

Sirilic permet de traiter en lot plusieurs couches et sessions.